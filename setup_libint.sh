#!/bin/bash

EXTERNAL=$PWD/external
LIBINT_export_dir=$PWD/libint_prep/libint_export
LIBINT_export=libint2
LIBINT_install_dir=$EXTERNAL/libint2
LIBINT_version=v2.7.1
GEN_compiler="g++"
GEN_optflags="-O2 -march=native -mavx"
CPPFLAGS='-I/opt/local/include'
LDFLAGS='-L/opt/local/lib'

usage() {
  echo "This downloads a generic libint library generated for yucca and installs it locally."
  echo "You may get better results by directly generating a library on your system."
  echo "In that case, see https://gitlab.com/team-parker/libint-yucca for a script to help."
  echo
  echo "Libint requires:"
  echo "  - libboost"
  echo "  - eigen3"
  echo
  echo "Setting up on ubuntu:"
  echo "  sudo apt install libboost-dev libeigen3"
  echo
  echo "Setting up on macOS with MacPorts:"
  echo "  sudo port install boost eigen3"
  echo
  echo "Usage:"
  echo "  $0 [options]"
  echo
  echo "Options:"
  printf "  %-20s %-80s\n" "-c <compiler>" "compiler used for generation (def: ${GEN_compiler})"
  printf "  %-20s %-80s\n" "-o <options>"  "optimization flags (def: ${GEN_optflags})"
  printf "  %-20s %-80s\n" "-i <includes>" "include options for compilation (def: ${CPPFLAGS})"
  printf "  %-20s %-80s\n" "-l <libraries>" "library options for compilation (def: ${LDFLAGS})"
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  usage
  exit
fi

while getopts ":c:o:i:l:" opt; do
  case ${opt} in
    c )
      GEN_compiler="$OPTARG"
      ;;
    o )
      GEN_optflags="$OPTARG"
      ;;
    i )
      CPPFLAGS="$OPTARG"
      ;;
    l )
      LDFLAGS="$OPTARG"
      ;;
    \? )
      echo "Invalid option: $OPTARG"
      ;;
    : )
      echo "Invalid option: $OPTARG required"
      ;;
  esac
done
shift $((OPTIND -1))

echo "Using libint2 options:"
echo "libint version: ${LIBINT_version}"
echo

echo "Using configure options:"
echo "Targeting compiler: ${GEN_compiler}"
echo "Targeting compiler optimization flags: ${GEN_optflags}"
echo "CPPFLAGS=${CPPFLAGS}"
echo "LDFLAGS=${LDFLAGS}"

set -euxo pipefail

mkdir -p libint_prep
cd libint_prep
curl -L -o libint2-${LIBINT_version}-${GEN_compiler}-generic.tgz \
  https://gitlab.com/team-parker/libint-yucca/-/jobs/artifacts/master/raw/libint2-${LIBINT_version}-${GEN_compiler}-generic.tgz?job=build
tar -xvzf libint2-${LIBINT_version}-${GEN_compiler}-generic.tgz
cd libint2-${LIBINT_version}-${GEN_compiler}-generic
CXXFLAGS="${CPPFLAGS} ${GEN_optflags}" LDFLAGS="$LDFLAGS" cmake . -DCMAKE_CXX_COMPILER="${GEN_compiler}" -DCMAKE_INSTALL_PREFIX="${LIBINT_install_dir}"
cmake --build . -j
cmake --build . --target install
