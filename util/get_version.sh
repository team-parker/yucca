#!/bin/bash

# Writes a file "version.cpp" with implementation info
# of the present version. Attempts to pull version name from git.
# Falls back to manual default if git is not available.

# This script is intended to be run from the root of the project.

usage() {
    echo "Usage: $0 [-h] [output_file]"
    echo "  output_file: file to write version info to (default: version.cpp)"
    echo "  -h: print this help message"
    exit 1
}

output="version.cpp"
if [ $# -gt 0 ]; then
    if [ "$1" == "-h" ]; then
        usage
    else
        output=$1
    fi
fi

GIT=$(which git)
IS_REPO="no"
if [ -n "$GIT" ]; then
    if [ $(git rev-parse --is-inside-work-tree) ]; then
        IS_REPO="yes"
    fi
fi

# defaults
MAJOR=0
MINOR=2
PATCH=0
DISTANCE=0
SHORT_HASH="unknown"
FULL_HASH="unknown"
BRANCH="unknown"
DIRTY="false"
TO_STRING="v0.2.0-0 unknown"

if [ -n "$GIT" ] && [ "$IS_REPO" == "yes" ]; then
    VERSION=$(git describe --tags --match "v[0-9]*.[0-9]*" --always --dirty)
    TO_STRING=$VERSION

    # check whether version starts with vX.Y.Z
    has_version=$(echo $VERSION | grep -E "^v[0-9]+\.[0-9]+\.[0-9]+")
    if [ -n "$has_version" ]; then
        VERSION=${VERSION:1} # remove leading "v"
        MAJOR=${VERSION%%.*}

        VERSION=${VERSION#*.}
        MINOR=${VERSION%%.*}

        VERSION=${VERSION#*.}
        PATCH=${VERSION%%-*}

        VERSION=${VERSION#*-}
        DISTANCE=${VERSION%%-*}

        VERSION=${VERSION#*-}
        SHORT_HASH=$(git rev-parse --short HEAD)

        VERSION=${VERSION#*-}
        if [ "$VERSION" == "dirty" ]; then
            DIRTY="true"
        else
            DIRTY="false"
        fi
    else
      SHORT_HASH=$VERSION
    fi

    FULL_HASH=$(git rev-parse HEAD)
    BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi

cat << EOF > ${output}.tmp
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \\file yucca/version.cpp.in
///
/// \\brief This file is used to generate the version.cpp file
#include <fmt/format.h>

#include <yucca/version.hpp>

namespace yucca::version
{

// clang-format off
const std::string to_string() { return "$TO_STRING"; }
unsigned int major() { return $MAJOR; }
unsigned int minor() { return $MINOR; }
unsigned int patch() { return $PATCH; }
unsigned int distance() { return $DISTANCE; }
const std::string short_hash() { return "$SHORT_HASH"; }
const std::string full_hash() { return "$FULL_HASH"; }
bool isdirty() { return $DIRTY; }
const std::string branch() { return "$BRANCH"; }
// clang-format on

const std::string description()
{
  const auto d = version::distance();
  if (d == 0) {
    return fmt::format("v{}.{}.{} {}{}", major(), minor(), patch(), short_hash(),
                       isdirty() ? " (dirty)" : "");
  } else {
    return fmt::format("v{}.{}.{}-{} {}{}", major(), minor(), patch(), d, short_hash(),
                       isdirty() ? " (dirty)" : "");
  }
}

} // namespace yucca::version

EOF

if ! cmp -s ${output}.tmp ${output}; then
    mv ${output}.tmp ${output}
else
    rm ${output}.tmp
fi
