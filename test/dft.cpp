#include <random>
#include <yucca_test.h>

#include <fmt/core.h>

#include <yucca/dft/density_functional.hpp>

#include "catch_amalgamated.hpp"

using namespace std;
using namespace yucca;
using namespace qleve;
using namespace Catch;

TEST_CASE("DFT", "[dft]")
{
  SECTION("VWN")
  {
    InputNode inp;
    inp.put<string>("functional", "vwn");
    DensityFunctional vwn(inp, 1);
    CHECK(vwn.needs_derivatives_up_to() == 0);
    CHECK(vwn.nxc_operations() == 1);
    CHECK(vwn.exchange_factor() == 0.0);
  }

  SECTION("PBE")
  {
    InputNode inp;
    inp.put<string>("functional", "pbe");
    DensityFunctional pbe(inp, 1);
    CHECK(pbe.needs_derivatives_up_to() == 1);
    CHECK(pbe.nxc_operations() == 2);
    CHECK(pbe.exchange_factor() == 0.0);
  }

  SECTION("PBE0")
  {
    InputNode inp;
    inp.put<string>("functional", "pbe0");
    DensityFunctional pbe0(inp, 1);

    CHECK(pbe0.needs_derivatives_up_to() == 1);
    CHECK(pbe0.nxc_operations() == 2);
    CHECK(pbe0.exchange_factor() == 0.25);
  }

  SECTION("B3LYP")
  {
    InputNode inp;
    inp.put<string>("functional", "b3lyp");
    DensityFunctional b3lyp(inp, 1);

    CHECK(b3lyp.needs_derivatives_up_to() == 1);
    CHECK(b3lyp.nxc_operations() == 2);
    CHECK(b3lyp.exchange_factor() == 0.2);
  }
}
