#include <yucca/input/read_input.hpp>
#include <yucca/resmf/ureshf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

static string examples = YUCCA_EXAMPLES_DIR;
static string molecules = YUCCA_MOLECULES_DIR;

shared_ptr<yucca::Method_> test_ureshf(string inp)
{
  InputNode input = yucca::read_input(examples + inp);
  auto scfinput = input.get_node("scf");

  auto molfile = scfinput.get<string>("molecule");
  Molecule molecule(molecules + molfile);
  auto basis = make_shared<BasisSet>(scfinput.get<string>("basis"), ".");
  auto dfbasis = scfinput.contains("df-basis") ?
                     make_shared<BasisSet>(scfinput.get<string>("df-basis"), ".") :
                     shared_ptr<BasisSet>();
  auto geometry = make_shared<Geometry>(molecule, basis, dfbasis);

  auto hf = get_method(scfinput, geometry);
  hf->compute();
  auto hf_wfn = hf->wavefunction();

  auto ureshfinput = input.get_node("ureshf");
  auto ureshf = get_method(ureshfinput, hf_wfn);

  return ureshf;
}

TEST_CASE("Unrestricted Resonating Hartree-Fock: H2O (def2-SVP)", "[uResHF]")
{
  libint2::initialize();

  SECTION("Testing uResHF SCF Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_ureshf("ureshf-matadj-conv-scf_H2O_def2-svp.yml");
    method->compute();

    auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

    if (!ureshf) {
      throw runtime_error("Dynamic cast of base method to uResHF method failed!");
    }

    auto energies = ureshf->energies();
    REQUIRE(energies[0] == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies[1] == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = ureshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing uResHF SCF convergence from a loaded wavefunction")
  {
    auto method = test_ureshf("ureshf-from-load-conv-scf_H2O_def2-svp.yml");
    method->compute();

    auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

    if (!ureshf) {
      throw runtime_error("Dynamic cast of base method to uResHF method failed!");
    }

    auto energies = ureshf->energies();
    REQUIRE(energies[0] == Approx(-85.26875221).epsilon(1e-5));
    REQUIRE(energies[1] == Approx(-84.46058148).epsilon(1e-5));

    double orb_gradient = ureshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }

  SECTION("Testing uResHF Triplet UHF Convergence")
  {
    auto method = test_ureshf("ureshf-ssUHF-conv-scf_H2O_triplet_def2-svp.yml");
    method->compute();

    auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

    if (!ureshf) {
      throw runtime_error("Dynamic cast of base method to uResHF method failed!");
    }

    auto energies = ureshf->energies();
    REQUIRE(energies[0] == Approx(-85.01509032).epsilon(1e-5));

    double orb_gradient = ureshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }
}

TEST_CASE("Unrestricted Resonating Hartree-Fock: 70deg twisted ethene (def2-SVP)", "[uResHF]")
{
  libint2::initialize();

  SECTION("Testing uResHF SCF Convergence with Matrix Adjugate Implementation")
  {
    auto method = test_ureshf("ureshf-matadj-conv-scf_ethene70_def2-svp.yml");
    method->compute();

    auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

    if (!ureshf) {
      throw runtime_error("Dynamic cast of base method to uResHF method failed!");
    }

    auto energies = ureshf->energies();
    REQUIRE(energies[0] == Approx(-111.32825297).epsilon(1e-5));
    REQUIRE(energies[1] == Approx(-111.12202013).epsilon(1e-5));

    double orb_gradient = ureshf->orb_gradient();
    REQUIRE(orb_gradient < 1e-5);
  }
}
