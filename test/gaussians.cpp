#include <random>

#include <qleve/checks.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor_contract.hpp>

#include <yucca/structure/solid_harmonics.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

TEST_CASE("Gaussian Transforms", "[gaussians]")
{
  SolidHarmonics sh;

  SECTION("d functions")
  {
    const auto& c2s = sh.cartesian_to_spherical(2);
    const auto& s2c = sh.spherical_to_cartesian(2);

    CHECK(c2s(2, 0) == Approx(-1.0 / 3.0));
    CHECK(c2s(4, 3) == Approx(-1.0 / 3.0 * std::sqrt(3.0)));

    CHECK(s2c(0, 2) == Approx(-0.5));
    CHECK(s2c(3, 4) == Approx(-0.5 * std::sqrt(3.0)));
  }

  SECTION("Spherical to Cartesian back to Spherical is identity")
  {
    for (int l = 0; l < 10; ++l) {
      const auto& c2s = sh.cartesian_to_spherical(l);
      const auto& s2c = sh.spherical_to_cartesian(l);
      auto s2c2s = qleve::gemm("n", "n", 1.0, c2s, s2c);

      const double degree_identity = qleve::linalg::ident(s2c2s);
      CHECK(degree_identity == Approx(0.0).margin(1e-12));
    }
  }
}
