#include <random>

#include <qleve/tensor.hpp>
#include <qleve/tensor_dot.hpp>

#include <yucca/dft/atomic_grid.hpp>
#include <yucca/dft/integrate_potential.hpp>
#include <yucca/dft/lebedev_laikov.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/dft/radial_quadrature.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/compute_densities.hpp>
#include <yucca/structure/molecule.hpp>
#include <yucca/util/libint_interface.hpp>

#include "catch_amalgamated.hpp"
#include "yucca_test.h"

using namespace std;
using namespace yucca;
using namespace Catch;

TEST_CASE("Lebedev Angular Grids", "[grids]")
{

  SECTION("6-point grid")
  {
    const int np = 6;

    auto [xyz, weights] = lebedev_angular_grid(np, 1.0);

    const double sum = std::accumulate(weights.data(), weights.data() + weights.size(), 0.0);

    CHECK(sum == Approx(1.0));

    CHECK(xyz(0, 0) == Approx(1.0));
    CHECK(xyz(1, 0) == Approx(0.0));
    CHECK(xyz(2, 0) == Approx(0.0));
    CHECK(weights(0) == Approx(0.1666666667));

    CHECK(xyz(0, 3) == Approx(0.0));
    CHECK(xyz(1, 3) == Approx(-1.0));
    CHECK(xyz(2, 3) == Approx(0.0));
    CHECK(weights(3) == Approx(0.1666666667));
  }

  SECTION("74-point grid")
  {
    const int np = 74;

    auto [xyz, weights] = lebedev_angular_grid(np, 1.0);

    const double sum = std::accumulate(weights.data(), weights.data() + weights.size(), 0.0);

    CHECK(sum == Approx(1.0));
  }
}


TEST_CASE("Radial Grids", "[grids]")
{

  SECTION("20-point integrate exp(-r)")
  {
    const int np = 20;
    const double alpha = 0.6;
    const double xi = 1.0;
    auto [points, weights] = radial_quadrature(RadialQuad::TA_M4, np, alpha, xi);

    qleve::Tensor<1> wexpr(qleve::exp(-1.0 * points) * weights);
    const double integrate = std::accumulate(wexpr.data(), wexpr.data() + wexpr.size(), 0.0);

    CHECK(integrate == Approx(2.000077163).epsilon(1e-6));
  }
}

TEST_CASE("Atomic Grids", "[grids]")
{
  const double pi = physical::pi<double>;

  SECTION("16 x 6 integrate exp(-r^2)")
  {
    const int nr = 16;
    const int nom = 6;

    std::vector<int> nang(nr, nom);

    CHECK(nang.size() == nr);

    auto [points, weights] =
        shelled_radialangular_grid({0.0, 0.0, 0.0}, nang, RadialQuad::TA_M4, 0.6, 1.0);

    const ptrdiff_t np = weights.size();

    double integrate = 0.0;
    for (ptrdiff_t i = 0; i < np; ++i) {
      const double x = points(0, i);
      const double y = points(1, i);
      const double z = points(2, i);

      const double fr = std::exp(-(x * x + y * y + z * z));
      integrate += fr * weights(i);
    }

    CHECK(integrate == Approx(std::pow(pi, 1.5)).epsilon(1e-6));
  }
}

TEST_CASE("Molecular Grids", "[grids]")
{
  const double pi = physical::pi<double>;

  SECTION("integrate sum of Gaussians using molecular grid")
  {
    yucca::Atom h1({0.0, 0.0, -0.5}, 1.0, 1.0, "h");
    yucca::Atom h2({0.0, 0.0, 0.5}, 1.0, 1.0, "h");

    InputNode inp;
    inp.put("grid level", 4ll);

    MolecularGrid molgrid(inp, {h1, h2});

    qleve::Tensor<2>& points = molgrid.points();
    qleve::Tensor<1>& weights = molgrid.weights();

    const ptrdiff_t np = weights.size();

    double integrate = 0.0;
    for (ptrdiff_t i = 0; i < np; ++i) {
      const double x = points(0, i);
      const double y = points(1, i);
      const double z = points(2, i);

      const double x1 = x;
      const double y1 = y;
      const double z1 = z - 0.5;

      const double x2 = x;
      const double y2 = y;
      const double z2 = z + 0.5;

      const double fr =
          std::exp(-(x1 * x1 + y1 * y1 + z1 * z1)) + std::exp(-(x2 * x2 + y2 * y2 + z2 * z2));
      integrate += fr * weights(i);
    }

    CHECK(integrate == Approx(2.0 * std::pow(pi, 1.5)).epsilon(1e-3));
    CHECK(integrate == Approx(11.1362656989).epsilon(1e-6));
  }
}

TEST_CASE("Molecular Integration", "[grids]")
{
  yucca::Atom o({0.0000000, 0.0000000, -0.0644484}, 8.0, 16.0, "o");
  yucca::Atom h1({0.7499151, 0.0000000, 0.5114913}, 1.0, 1.0, "h");
  yucca::Atom h2({-0.7499151, 0.0000000, 0.5114913}, 1.0, 1.0, "h");

  Molecule water({o, h1, h2});

  auto tzvp = make_shared<BasisSet>("def2-tzvp", ".");
  auto geom = make_shared<Geometry>(water, tzvp, nullptr);

  const ptrdiff_t nao = geom->orbital_basis()->nbasis();

  CHECK(nao == 43);

  InputNode inp;
  inp.put("prune grid", false);
  inp.put("grid level", 3ll);

  MolecularGrid molgrid(inp, water.atoms());

  const ptrdiff_t np = molgrid.weights().size();
  CHECK(np == 18760);

  SECTION("integrate norms of SAO using density integration")
  {
    qleve::Tensor<2> rho(nao, nao);
    for (ptrdiff_t i = 0; i < nao; ++i) {
      rho = 0.0;
      rho(i, i) = 1.0;

      auto densities = compute_densities(*geom->orbital_basis(), rho, molgrid.points());
      const double integrated_density = qleve::dot_product(densities.pluck(0), molgrid.weights());

      CHECK(integrated_density == Approx(1.0).epsilon(2e-4));
    }
  }

  SECTION("integrate multipole moments using integrate_potential")
  {
    libint2::initialize();

    auto mu = geom->multipole(1);
    auto mux = mu.pluck(1);

    auto xfunc = [](const double x, const double y, const double z,
                    qleve::ConstTensorView<1> dens) { return x; };

    qleve::Tensor<2> densities(np, 1); // dummy densities

    auto numx = dft::integrate_potential(*geom->orbital_basis(), molgrid, densities, xfunc);

    qleve::Tensor<2> serr = qleve::abs(mux - numx);
    const double serrnrm = serr.norm();
    CHECK(serrnrm == Approx(0.0).margin(1e-3));

    libint2::finalize();
  }
}
