// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/density_fitting.hpp
///
/// Density Fitting class
#pragma once

#include <yucca/structure/basis.hpp>

namespace yucca
{

class DensityFitting {
 protected:
  std::shared_ptr<Basis> orbital_basis_;
  std::shared_ptr<Basis> fitting_basis_;
  std::unique_ptr<qleve::Tensor<3>> Coulomb3C_;
  std::unique_ptr<qleve::Tensor<2>> metric_;
  std::unique_ptr<qleve::Tensor<2>> metric_cholesky_;
  std::unique_ptr<qleve::Tensor<3>> B_;
  std::unique_ptr<qleve::Tensor<3>> B_Jinv_;

  void fill_2e3c();
  void fill_2e2c();

 public:
  DensityFitting(const std::shared_ptr<Basis>& orbital_basis,
                 const std::shared_ptr<Basis>& fitting_basis);

  const std::shared_ptr<Basis> orbital_basis() const { return orbital_basis_; }
  const std::shared_ptr<Basis> fitting_basis() const { return fitting_basis_; }

  const qleve::Tensor<3>& Coulomb3C() { return *Coulomb3C_; } // (uv|P)
  const qleve::Tensor<2>& metric() { return *metric_; }
  const qleve::Tensor<2>& metric_cholesky() { return *metric_cholesky_; }
  const qleve::Tensor<3>& B() { return *B_; } // (uv|Q)(Q|P)^(-1/2)

  qleve::Tensor<3> compute_B_Jinv(); // (uv|Q)(Q|P)^(-1/2)J^(-1/2)o
};

} // namespace yucca
