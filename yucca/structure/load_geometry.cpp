// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/load_geometry.hpp
///
/// Read from input file to load molecular structure, basis sets, etc.

#include <yucca/structure/basis.hpp>
#include <yucca/structure/basisset.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/structure/load_geometry.hpp>
#include <yucca/wfn/wfn.hpp>

using namespace yucca;
using namespace std;

tuple<shared_ptr<Geometry>, shared_ptr<Wfn>> yucca::load_geometry(const InputNode& input,
                                                                  shared_ptr<Geometry> geometry,
                                                                  shared_ptr<Wfn> wfn)
{
  using basptr = shared_ptr<Basis>;
  using setptr = shared_ptr<BasisSet>;

  assert(!wfn || (wfn->geometry() == geometry) && "expecting input geometry and wfn to match");
  if (wfn && wfn->geometry()) {
    geometry = wfn->geometry();
  }
  shared_ptr<Molecule> molecule = geometry ? static_pointer_cast<Molecule>(geometry) : nullptr;

  basptr orbital_basis = geometry ? geometry->orbital_basis() : nullptr;
  setptr basisset = orbital_basis ? orbital_basis->basisset() : nullptr;

  basptr fitting_basis = geometry ? geometry->fitting_basis() : nullptr;
  setptr dfbasisset = fitting_basis ? fitting_basis->basisset() : nullptr;

  basptr j_fitting_basis = geometry ? geometry->j_fitting_basis() : nullptr;
  setptr j_dfbasisset = j_fitting_basis ? j_fitting_basis->basisset() : nullptr;

  basptr k_fitting_basis = geometry ? geometry->k_fitting_basis() : nullptr;
  setptr k_dfbasisset = k_fitting_basis ? k_fitting_basis->basisset() : nullptr;

  if (input.contains("molecule")) {
    molecule = make_shared<yucca::Molecule>(input);

    // is new molecule compatible with previous geometry?
    if (geometry) {
      if (!equal(molecule->atoms().begin(), molecule->atoms().end(), geometry->atoms().begin(),
                 [](const yucca::Atom& n, const yucca::Atom& o) {
                   return n.symbol() == o.symbol();
                 })) {
        geometry.reset(); // if not, geometry needs to be reset
        wfn.reset();
      }
    }
  } else if (molecule) {
    fmt::print("Continuing with previously defined molecule.\n");
  } else {
    throw runtime_error("No molecule has been specified!");
  }

  if (input.contains("basis")) {
    string basisnick = input.get<string>("basis");
    basisset = make_shared<yucca::BasisSet>(basisnick, ".");
    orbital_basis = make_shared<yucca::Basis>(molecule->atoms(), basisset);
  } else if (basisset) {
    fmt::print("Continuing with basis {}\n", basisset->nick());
  } else {
    throw runtime_error("No basis has been specified!");
  }

  if (input.contains("df-basis")) {
    if (auto dfb = input.get_node("df-basis"); dfb.is_string()) {
      string basisnick = input.get<string>("df-basis");
      if (basisnick != "") {
        dfbasisset = make_shared<yucca::BasisSet>(basisnick, ".");
        fitting_basis = make_shared<yucca::Basis>(molecule->atoms(), dfbasisset);
      } else {
        fitting_basis.reset();
        dfbasisset.reset();
      }

      // null out j/kfitting_basis if single fitting basis is provided
    } else if (dfb.is_vector()) {
      auto dfbs = input.get_vector<string>("df-basis");
      if (dfbs.size() != 2) {
        throw runtime_error("df-basis should be a string or a vector of 2 strings");
      }
      string j_basisnick = dfbs[0];
      if (j_basisnick != "") {
        j_dfbasisset = make_shared<yucca::BasisSet>(j_basisnick, ".");
        j_fitting_basis = make_shared<yucca::Basis>(molecule->atoms(), j_dfbasisset);
      } else {
        j_fitting_basis.reset();
        j_dfbasisset.reset();
      }

      string k_basisnick = dfbs[1];
      if (k_basisnick != "") {
        k_dfbasisset = make_shared<yucca::BasisSet>(k_basisnick, ".");
        k_fitting_basis = make_shared<yucca::Basis>(molecule->atoms(), k_dfbasisset);
      } else {
        k_fitting_basis.reset();
        k_dfbasisset.reset();
      }

      // null out fitting_basis if either of these are provided
      dfbasisset.reset();
      fitting_basis.reset();
    } else {
      throw runtime_error("df-basis should be a string or a vector of 2 strings");
    }
  } else if (dfbasisset) {
    fmt::print("Continuing with fitting basis {}\n", dfbasisset->nick());
  }

  if (!geometry) { // make a new object
    geometry = make_shared<yucca::Geometry>(*molecule, orbital_basis, fitting_basis,
                                            j_fitting_basis, k_fitting_basis);
  } else if (input.contains_any({"molecule", "basis", "df-basis"})) { // update existing geometry
    geometry = make_shared<yucca::Geometry>(*molecule, orbital_basis, fitting_basis,
                                            j_fitting_basis, k_fitting_basis);
    if (wfn) {
      wfn = wfn->update(geometry);
    }
  }

  return make_tuple(geometry, wfn);
}

