// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/basisset.hpp
///
/// Class to describe basis set information (not basis)
#pragma once

#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace yucca
{

/// Exponents and coefficients for single angular momentum shell
class ShellSet {
 protected:
  std::vector<double> exponents_;
  std::vector<double> coeffs_;
  int angmom_;

 public:
  ShellSet(const std::vector<double>& ex, const std::vector<double>& co, const int l) :
      exponents_(ex), coeffs_(co), angmom_(l)
  {}

  const std::vector<double>& exponents() const { return exponents_; }
  const std::vector<double>& coeffs() const { return coeffs_; }
  int angmom() const { return angmom_; }
};

/// holds a map from atom name to list of ShellSets
class BasisSet {
 protected:
  using AtomSet = std::vector<ShellSet>;

 protected:
  std::map<std::string, AtomSet> basis_map_;

  std::string nick_;

  std::string find_library(const std::string& nick,
                           const std::vector<std::string>& libraries) const;

 public:
  BasisSet(const std::string& nick, const std::string& config);

  const AtomSet& atom_set(const std::string& symbol) const { return basis_map_.at(symbol); }
  const std::string& nick() const { return nick_; }

  void print(std::ostream& os = std::cout, const std::vector<std::string>& elements = {}) const;
};

} // namespace yucca
