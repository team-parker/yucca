// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/geometry.hpp
///
/// Geometry classes
#pragma once

namespace yucca
{
class Basis;          // #include <yucca/structure/basis.hpp>
class BasisSet;       // #include <yucca/structure/basisset.hpp>
class DensityFitting; // #include <yucca/structure/density_fitting.hpp>
class MolecularGrid;  // #include <yucca/dft/molecular_grid.hpp>
} // namespace yucca

#include <yucca/structure/molecule.hpp>

namespace yucca
{

class Geometry : public Molecule {
 protected:
  std::shared_ptr<Basis> orbital_basis_;
  std::shared_ptr<Basis> fitting_basis_;
  std::shared_ptr<Basis> j_fitting_basis_;
  std::shared_ptr<Basis> k_fitting_basis_;

  std::shared_ptr<DensityFitting> density_fitting_;
  std::shared_ptr<DensityFitting> j_density_fitting_;
  std::shared_ptr<DensityFitting> k_density_fitting_;

  std::shared_ptr<MolecularGrid> molecular_grid_;

 public:
  Geometry(const Molecule& mol, const std::shared_ptr<BasisSet>& orbital_basisset,
           const std::shared_ptr<BasisSet>& fitting_basisset,
           const std::shared_ptr<BasisSet>& j_fitting_basisset = nullptr,
           const std::shared_ptr<BasisSet>& k_fitting_basisset = nullptr);
  Geometry(const Molecule& mol, const std::shared_ptr<Basis>& orbital_basis,
           const std::shared_ptr<Basis>& fitting_basis,
           const std::shared_ptr<Basis>& j_fitting_basis = nullptr,
           const std::shared_ptr<Basis>& k_fitting_basis = nullptr);

  Geometry update(const qleve::ConstTensorView<2> coords) const;

  const std::shared_ptr<Basis>& orbital_basis() const { return orbital_basis_; }
  const std::shared_ptr<Basis>& fitting_basis() const { return fitting_basis_; }
  const std::shared_ptr<Basis>& j_fitting_basis() const { return j_fitting_basis_; }
  const std::shared_ptr<Basis>& k_fitting_basis() const { return k_fitting_basis_; }

  const std::shared_ptr<DensityFitting>& density_fitting() const { return density_fitting_; }
  const std::shared_ptr<DensityFitting>& j_density_fitting() const { return j_density_fitting_; }
  const std::shared_ptr<DensityFitting>& k_density_fitting() const { return k_density_fitting_; }
  const std::shared_ptr<MolecularGrid>& molecular_grid() const { return molecular_grid_; }

  qleve::Tensor<2> overlap() const;
  qleve::Tensor<2> hcore(const bool force_no_efield = false) const;
  qleve::Tensor<2> kinetic() const;
  qleve::Tensor<2> nuclear_attraction() const;
  qleve::Tensor<3> multipole(const int& order,
                             const std::array<double, 3> origin = {0.0, 0.0, 0.0}) const;
};

} // namespace yucca
