// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/compute_densities.hpp
///
/// compute densities on points classes
#pragma once

#include <qleve/tensor.hpp>

#include <yucca/structure/basis.hpp>

namespace yucca
{

/// Compute densities on a set of points
///
/// \return a tensor of shape {npoints, 1}
qleve::Tensor<2> compute_densities(const Basis& basis, const qleve::ConstTensorView<2>& rho,
                                   const qleve::ConstTensorView<2>& xyz);

/// Compute densities and generalized gradients on a set of points
///
/// \return a tensor of shape {npoints, 4}
///        where the last index is 0 for the density and 1-3 for the gradients in xyz
qleve::Tensor<2> compute_densities_gradients(const Basis& basis,
                                             const qleve::ConstTensorView<2>& rho,
                                             const qleve::ConstTensorView<2>& xyz);

} // namespace yucca
