// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/structure/geometry_properties.cpp
///
/// A brief description of the contents of this file
#include <algorithm>
#include <iostream>
#include <vector>

#include <fmt/core.h>

#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/libint_interface.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

Tensor<2> Geometry::overlap() const
{
  Tensor<2> out(orbital_basis_->nbasis(), orbital_basis_->nbasis());

  libint::fill_1e(libint2::Operator::overlap, *this->orbital_basis_, out);

  return out;
}

Tensor<2> Geometry::hcore(const bool force_no_efield) const
{
  Tensor<2> out(orbital_basis_->nbasis(), orbital_basis_->nbasis());

  libint::fill_1e(libint2::Operator::kinetic, *this->orbital_basis_, out);
  libint::fill_1e(libint2::Operator::nuclear, *this->orbital_basis_, out, atoms());

  if (!efield_.empty() > 0 && !force_no_efield
      && std::any_of(efield_.begin(), efield_.end(), [](const double e) { return e != 0.0; })) {
    if (efield_.size() != 3) {
      throw std::runtime_error("electric field must be length 3");
    }
    fmt::print("adding multipole!\n");
    const auto multip = this->multipole(1);
    for (int i = 1; i < 4; ++i) {
      out += efield_.at(i - 1) * multip.const_pluck(i);
    }
  }

  return out;
}

Tensor<2> Geometry::kinetic() const
{
  Tensor<2> out(orbital_basis_->nbasis(), orbital_basis_->nbasis());

  libint::fill_1e(libint2::Operator::kinetic, *this->orbital_basis_, out);

  return out;
}

Tensor<2> Geometry::nuclear_attraction() const
{
  Tensor<2> out(orbital_basis_->nbasis(), orbital_basis_->nbasis());

  libint::fill_1e(libint2::Operator::nuclear, *this->orbital_basis_, out, atoms());

  return out;
}

Tensor<3> Geometry::multipole(const int& order, std::array<double, 3> origin) const
{
  assert(order > 0 && order <= 3);

  int nops;
  decltype(libint2::Operator::emultipole1) op;
  if (order == 1) {
    nops = 4;
    op = libint2::Operator::emultipole1;
  } else if (order == 2) {
    nops = 10;
    op = libint2::Operator::emultipole2;
  } else if (order == 3) {
    nops = 20;
    op = libint2::Operator::emultipole3;
  }

  Tensor<3> out(orbital_basis_->nbasis(), orbital_basis_->nbasis(), nops);

  libint::compute_1e_stack_impl(op, nops, *this->orbital_basis_, out);

  return out;
}
