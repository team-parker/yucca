// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/properties/properties.hpp
///
/// Routines to compute properties from the 1RDM (should this move?)
#pragma once

#include <qleve/tensor.hpp>

namespace yucca
{

class InputNode;
class Geometry;

namespace properties
{

/// Driver routine to compute properties from the 1RDM
void properties(const InputNode& input, const std::shared_ptr<Geometry>& geometry,
                const qleve::ConstTensorView<2>& density);

} // namespace properties
} // namespace yucca
