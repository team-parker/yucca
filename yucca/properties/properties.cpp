// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/properties/properties.cpp
///
/// Routines to compute properties from the 1RDM (should this move?)

#include <fmt/core.h>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/io/gaussian_cube.hpp>
#include <yucca/properties/properties.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

/// Driver routine to compute properties from the 1RDM
void yucca::properties::properties(const InputNode& input,
                                   const std::shared_ptr<Geometry>& geometry,
                                   const qleve::ConstTensorView<2>& density)
{
  fmt::print("Computing properties\n");

  if (input.contains("cube")) {
    fmt::print("Writing density to cube file\n");
    const auto filename = input.get_node("cube").get<string>("filename", "density.cube");
    io::write_density_to_cube(input.get_node("cube"), geometry, density, filename);
  }
}

