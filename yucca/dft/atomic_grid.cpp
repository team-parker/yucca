// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/atomic_grid.cpp
///
/// Atom-centered grids for DFT integration

#include <yucca/dft/atomic_grid.hpp>
#include <yucca/dft/lebedev_laikov.hpp>
#include <yucca/physical/constants.hpp>

using namespace yucca;
using namespace std;
using namespace qleve;

void yucca::shelled_radialangular_grid(const std::array<double, 3>& center,
                                       const std::vector<int>& nang, const RadialQuad& quadrature,
                                       const double& alpha, const double& xi,
                                       qleve::TensorView<2> points, qleve::TensorView<1> weights)
{
  const ptrdiff_t np_t = std::accumulate(nang.begin(), nang.end(), 0ll);

  assert(points.extent(0) == 3 && points.extent(1) == np_t);
  assert(weights.size() == np_t);

  const double pi4 = 4.0 * physical::pi<double>;

  ptrdiff_t i = 0;

  const ptrdiff_t nr = nang.size();

  auto [radial_points, radial_weights] = radial_quadrature(quadrature, nr, alpha, xi);

  for (ptrdiff_t ir = 0; ir < nr; ++ir) {
    const double r = radial_points(ir);
    const double wr = radial_weights(ir);
    const ptrdiff_t npang = nang.at(ir);

    auto [angular_points, angular_weights] = lebedev_angular_grid(npang, r, center);

    points.subtensor({0, i}, {3, i + npang}) = angular_points;
    weights.subtensor({i}, {i + npang}) = angular_weights * wr * pi4;

    i += npang;
  }
}

std::tuple<qleve::Tensor<2>, qleve::Tensor<1>> yucca::shelled_radialangular_grid(
    const std::array<double, 3>& center, const std::vector<int>& nang, const RadialQuad& quadrature,
    const double& alpha, const double& xi)
{
  const ptrdiff_t np_t = std::accumulate(nang.begin(), nang.end(), 0ll);

  qleve::Tensor<2> points(3, np_t);
  qleve::Tensor<1> weights(np_t);

  shelled_radialangular_grid(center, nang, quadrature, alpha, xi, points, weights);

  return std::make_tuple(points, weights);
}
