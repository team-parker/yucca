set(DFT_SOURCES
  atomic_grid.cpp
  lebedev_laikov.cpp
  radial_quadrature.cpp
  standard_grids.cpp
  molecular_grid.cpp
  partitioning.cpp
  libxc_interface.cpp
  density_functional.cpp
  integrate_functional.cpp
  )

add_library(dft ${DFT_SOURCES})
target_include_directories(dft PRIVATE $<TARGET_PROPERTY:Libxc::xc,INTERFACE_INCLUDE_DIRECTORIES>)
target_link_libraries(dft PUBLIC structure qleve fmt::fmt tbb Libxc::xc)
