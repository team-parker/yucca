// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/integrate_functional.hpp
///
/// Integrate a DensityFunctional object outputs an SAO basis
#pragma once

#include <qleve/tensor.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/dft/molecular_grid.hpp>
#include <yucca/structure/evaluate_cao.hpp>
#include <yucca/util/distribute_tasks.hpp>

namespace yucca::dft
{

/// Integrates Exc and Vxc.
///
/// returns [Exc, Vxc(nsao, nsao)]
std::tuple<double, qleve::Tensor<2>> integrate_exc_vxc(
    const Basis& basis, const MolecularGrid& grid, const DensityFunctional& df,
    const qleve::ConstTensorView<2> rho, qleve::TensorView<2> density, const int batchsize = 512,
    const double thresh = 1e-7);


} // namespace yucca::dft
