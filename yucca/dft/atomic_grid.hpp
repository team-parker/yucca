// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/atomic_grid.hpp
///
/// Atom-centered grids for DFT integration
#pragma once

#include <array>
#include <vector>

#include <qleve/tensor.hpp>

#include <yucca/dft/radial_quadrature.hpp>

namespace yucca
{

void shelled_radialangular_grid(const std::array<double, 3>& center, const std::vector<int>& nang,
                                const RadialQuad& quadrature, const double& alpha, const double& xi,
                                qleve::TensorView<2> points, qleve::TensorView<1> weights);

std::tuple<qleve::Tensor<2>, qleve::Tensor<1>> shelled_radialangular_grid(
    const std::array<double, 3>& center, const std::vector<int>& nang, const RadialQuad& quadrature,
    const double& alpha, const double& xi);

} // namespace yucca
