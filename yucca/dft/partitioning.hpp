// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/partitioning.hpp
///
/// Becke partitioning
#pragma once

#include <array>

#include <yucca/structure/geometry.hpp>

namespace yucca
{

class InputNode;

class BeckePartition_ {
 public:
  virtual double weight(const ptrdiff_t& region, const double& x, const double& y,
                        const double& z) = 0;
};

/// Implementation based on https://doi.org/10.1002/jcc.23323
class BeckeADFPartition : public BeckePartition_ {
 protected:
  std::unique_ptr<qleve::Tensor<2>> centers_;
  std::unique_ptr<qleve::Tensor<1>> etas_;
  ptrdiff_t nregions_;

 public:
  BeckeADFPartition(const yucca::InputNode& input, const std::vector<Atom>& atoms);

  double weight(const ptrdiff_t& region, const double& x, const double& y,
                const double& z) override;

 protected:
  double nonnormed_partition(const ptrdiff_t& region, const double& x, const double& y,
                             const double& z);
};

std::shared_ptr<BeckePartition_> create_partitions(const yucca::InputNode& input,
                                                   const std::vector<Atom>& atoms);

} // namespace yucca
