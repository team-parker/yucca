// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/dft/standard_grids.hpp
///
/// Definitions of "standard" grids, like the Ahlrichs grids
#pragma once

#include <vector>

namespace yucca
{

std::vector<int> get_standard_grid_sizes(const int row, const int grid_level, const bool prune);

std::vector<int> get_standard_grid_npoints(const int row, const int grid_level, const bool prune);

double get_treutler_ahlrichs_xi(const int element_charge);

} // namespace yucca
