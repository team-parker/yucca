// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/method.hpp
///
/// Method base class that provides a unified handle for all methods
#pragma once

#include <memory>

#include <qleve/tensor.hpp>

#include <yucca/input/input_node.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry;  // #include <yucca/structure/geometry.hpp>
class Wfn;       // #include <yucca/wfn/wfn.hpp>
class InputNode; // #include <yucca/input/input_node.hpp>

class Method_ {
 protected:
  std::shared_ptr<Geometry> geometry_;
  InputNode input_;

  std::unique_ptr<qleve::Tensor<2>> gradient_; ///< dE/dR

 public:
  Method_(const InputNode& input, const std::shared_ptr<Geometry> g);
  Method_(const Method_&);

  virtual void compute() = 0;
  virtual double energy() = 0;
  virtual std::vector<double> energies() = 0;

  qleve::Tensor<2> gradient();

  virtual std::shared_ptr<Wfn> wavefunction() = 0;

 protected:
  virtual void compute_gradient_impl() = 0;
};

std::shared_ptr<Method_> get_method(const InputNode& input, const std::shared_ptr<Geometry>& g);
std::shared_ptr<Method_> get_method(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

} // namespace yucca
