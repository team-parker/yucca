// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/util/timer.hpp
///
/// Convenience routine to simplify timing routines
#pragma once

#include <chrono>
#include <string>
#include <unordered_map>

namespace yucca
{

class Timer {
 protected:
  using tp = decltype(std::chrono::high_resolution_clock::now());
  tp now_;
  std::string title_;
  std::unordered_map<std::string, std::chrono::microseconds> times_;

 public:
  Timer(const std::string title);

  double tick(const std::string& section);
  void mark();
  void tick_print(const std::string& section);

  void summarize();
};

} // namespace yucca
