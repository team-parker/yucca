// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/scf.cpp
///
/// SCF driver class
#include <fmt/core.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_phase_adjust.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

#include <yucca/dft/density_functional.hpp>
#include <yucca/dft/integrate_functional.hpp>
#include <yucca/dft/molecular_grid.hpp>
#include <yucca/input/input_node.hpp>
#include <yucca/properties/properties.hpp>
#include <yucca/scf/converger.hpp>
#include <yucca/scf/fock.hpp>
#include <yucca/scf/initial_guess.hpp>
#include <yucca/scf/scf.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/timer.hpp>
#include <yucca/wfn/single_reference.hpp>

using namespace std;
using namespace yucca;

yucca::SCF::SCF(const SCF& x) : Method_(x)
{
  this->nmo_ = x.nmo_;
  this->nao_ = x.nao_;
  this->nocc_ = x.nocc_;
  this->nelec_ = x.nelec_;
  this->charge_ = x.charge_;

  this->overlap_ = std::make_shared<qleve::Tensor<2>>(*x.overlap_);
  this->hcore_ = std::make_shared<qleve::Tensor<2>>(*x.hcore_);
  this->coeffs_ = std::make_shared<qleve::Tensor<2>>(*x.coeffs_);
  this->rho_ = std::make_shared<qleve::Tensor<2>>(*x.rho_);
  this->fock_ = std::make_shared<qleve::Tensor<2>>(*x.fock_);
  this->eigs_ = std::make_shared<qleve::Tensor<1>>(*x.eigs_);

  this->max_iter_ = x.max_iter_;
  this->conv_energy_ = x.conv_energy_;
  this->conv_error_ = x.conv_error_;
  this->conv_density_ = x.conv_density_;
}


yucca::SCF::SCF(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(input, g)
{
  fmt::print("Setting up SCF calculation\n");

  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  charge_ = input.get<ptrdiff_t>("charge", 0);
  nelec_ = geometry_->total_nuclear_charge() - charge_;
  nao_ = geometry_->orbital_basis()->nbasis();

  // this is an important block: set it up rigorously externally somewhere
  string conv_key = input.get_if_value<string>("convergence", "default");
  if (conv_key == "loose") {
    conv_energy_ = 1e-6;
    conv_error_ = 1e-3;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "default") {
    conv_energy_ = 1e-7;
    conv_error_ = 1e-5;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "tight") {
    conv_energy_ = 1e-8;
    conv_error_ = 1e-6;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "ultratight") {
    conv_energy_ = 1e-9;
    conv_error_ = 1e-7;
    conv_density_ = 1e10; // TODO
  }

  // allow specific overrides
  conv_energy_ = input.get<double>("conv_energy", conv_energy_);
  conv_error_ = input.get<double>("conv_error", conv_error_);
  conv_density_ = input.get<double>("conv_density", conv_density_);

  fmt::print("Convergence criteria:\n");
  fmt::print("  - change in energy (dE) < {:1e}\n", conv_energy_);
  fmt::print("  - gradient norm (grad) < {:1e}\n", conv_error_);
  fmt::print("  - change in density (drho) < {:1e}\n", conv_density_);
  fmt::print("\n");

  auto dft = input.get_node_if("dft");
  if (dft) {
    dft_ = make_shared<DensityFunctional>(*dft, 1);
    molgrid_ = make_shared<MolecularGrid>(*dft, geometry_->atoms());
    density_ = make_shared<qleve::Tensor<2>>(molgrid_->weights().size(), dft_->nxc_operations());

    dft_batch_ = dft->get_if_map<ptrdiff_t>("batch size", 512);
    dft_thresh_ = dft->get_if_map<double>("density threshold", 1e-7);

    dft_->print_header();
  }

  if (nelec_ % 2 != 0) {
    throw runtime_error("Number of electrons must be even for restricted calculation!");
  }
  nocc_ = nelec_ / 2;

  fmt::print("Electronic configuration:\n");
  fmt::print("  - nelec: {:d}\n", nelec_);
  fmt::print("  - nocc: {:d}\n", nocc_);

  guess_ = input.get<string>("guess", "atomic");

  converger_ = make_unique<RHFConverger>(input, nocc_);
}

yucca::SCF::SCF(const InputNode& input, const shared_ptr<Wfn>& wfn) : SCF(input, wfn->geometry())
{
  const shared_ptr<SingleReference> ref = dynamic_pointer_cast<SingleReference>(wfn);

  if (ref) {
    coeffs_ = make_shared<qleve::Tensor<2>>(*ref->coeffs());
    // coeffs_ = ref->coeffs();
    nmo_ = coeffs_->extent(1);

    rho_ = make_shared<qleve::Tensor<2>>(nao_, nao_);

    qleve::TensorView<2> Cocc = coeffs_->slice(0, nocc_); // occupied orbitals
    qleve::gemm("n", "t", 1.0, Cocc, Cocc, 0.0, *rho_);
  }
}

void yucca::SCF::compute()
{
  Timer timer("SCF");

  // Set up required quantities
  if (!hcore_)
    hcore_ = make_shared<qleve::Tensor<2>>(geometry_->hcore()); // hcore in AO
  if (!overlap_)
    overlap_ = make_shared<qleve::Tensor<2>>(geometry_->overlap()); // overlap in AO

  if (!rho_) {
    timer.mark();
    if (coeffs_) {
      fmt::print("Generating initial guess from provided orbitals\n");
      // assert(false);

      nmo_ = coeffs_->extent(1);

      rho_ = make_shared<qleve::Tensor<2>>(nao_, nao_);
      qleve::TensorView<2> Cocc = coeffs_->slice(0, nocc_); // occupied orbitals
      qleve::gemm("n", "t", 1.0, Cocc, Cocc, 0.0, *rho_);
    } else if (guess_ == "hcore") {
      fmt::print("Generating initial guess from hcore\n");
      coeffs_ = make_shared<qleve::Tensor<2>>(initial_guess::hcore(*overlap_, *hcore_));

      nmo_ = coeffs_->extent(1);

      rho_ = make_shared<qleve::Tensor<2>>(nao_, nao_);
      qleve::TensorView<2> Cocc = coeffs_->slice(0, nocc_); // occupied orbitals
      qleve::gemm("n", "t", 1.0, Cocc, Cocc, 0.0, *rho_);
    } else if (guess_ == "atomic") {
      fmt::print("Generating initial guess from sum of atomic densities\n");

      rho_ = make_shared<qleve::Tensor<2>>(initial_guess::atomic_densities(*geometry_));
      coeffs_ =
          make_shared<qleve::Tensor<2>>(qleve::linalg::orthogonalize_metric(*overlap_, 1.0e-10));
      nmo_ = coeffs_->extent(1);

      // purify rho_ and replace coeffs_ TODO: separate this as a function?
      qleve::Tensor<2> SC(nao_, nmo_);
      qleve::gemm("n", "n", 1.0, *overlap_, *coeffs_, 0.0, SC);
      qleve::Tensor<2> rhomo(nmo_, nmo_);
      // multiply by -1 so that the largest eigenvalues are sorted first
      qleve::matrix_transform(-1.0, *rho_, SC, 0.0, rhomo);
      auto rhoeigs = qleve::linalg::diagonalize(rhomo);

      qleve::gemm("n", "n", 1.0, *coeffs_, rhomo, 0.0, SC);
      *coeffs_ = SC;
      qleve::gemm("n", "t", 1.0, coeffs_->slice(0, nocc_), coeffs_->slice(0, nocc_), 0.0, *rho_);
    }

    const double nelec = qleve::dot_product(nao_ * nao_, rho_->data(), overlap_->data()) * 2.0;
    fmt::print("Initial density: {:12.4f} electrons\n", nelec);
    timer.tick_print("initial guess");
  }

  fmt::print("  - nmo: {:d}\n\n", nmo_);

  fmt::print("Starting SCF calculation\n");

  const double nuclear_repulsion = geometry_->nuclear_repulsion();

  if (!fock_builder_) {
    fock_builder_ =
        make_unique<RFockBuilder>(*geometry_, 1.0, dft_ ? dft_->exchange_factor() : 1.0);
  }

  double last_energy;
  bool conv = false;

  fmt::print("{:>4s} {:>16s} {:>16s} {:>16s} {:>10s} {:>10s} {:>8s}\n", "iter", "1e", "2e",
             "energy", "error", "dE", "time");
  fmt::print("{:-^90s}\n", "");

  qleve::Tensor<2> fock(nao_, nao_);
  qleve::Tensor<1> occs(nocc_);
  occs = 1.0;
  timer.mark();
  for (size_t iscf = 0; iscf < max_iter_; ++iscf) {
    fock = *hcore_;
    (*fock_builder_)(coeffs_->slice(0, nocc_), occs, *rho_, fock);

    double focktime = timer.tick("fock build");

    // compute energies
    const double energy0 =
        2.0
        * qleve::dot_product(rho_->size(), rho_->data(),
                             fock.data()); // zeroth-order energy (sum of eigenvalues)
    const double energy1e = 2.0 * qleve::dot_product(rho_->size(), rho_->data(), hcore_->data());
    const double energy2e = 0.5 * (energy0 - energy1e); // 2e energy
    energy_ = 0.5 * (energy1e + energy0) + nuclear_repulsion;

    if (dft_) {
      auto [Exc, Vxc] = dft::integrate_exc_vxc(*geometry_->orbital_basis(), *molgrid_, *dft_, *rho_,
                                               *density_, dft_batch_, dft_thresh_);

      double vxctime = timer.tick("Vxc build");
      focktime += vxctime;

      energy_ += Exc;
      fock += Vxc;
    }

    qleve::Tensor<2> err(fock.shape());
    qleve::Tensor<2> tmp(fock.shape());
    qleve::gemm("n", "n", 1.0, fock, *rho_, 0.0, tmp);
    qleve::gemm("n", "n", 1.0, tmp, *overlap_, 0.0, err);
    err = err - err.transpose();
    const double error_norm = err.norm();

    converger_->push(fock, err, energy_);

    const double dE = converger_->dE();
    last_energy = energy_;

    // print iteration
    fmt::print("{:4d} {:16.8f} {:16.8f} {:16.8f} {:10.1e} ", iscf, energy1e, energy2e, energy_,
               error_norm);
    if (iscf != 0) {
      fmt::print("{:10.2e}", dE);
    } else {
      fmt::print("{:>10s}", "---");
    }
    fmt::print(" {:8.3f}\n", focktime);

    conv = converged(dE, error_norm, 0.0);

    if (conv) {
      break;
    }

    converger_->next(*coeffs_, *rho_);
  }


  if (conv) {
    fmt::print("\n\n  Converged!\n");
    fmt::print("Final energy: {:20.8f}\n", energy_);
    fmt::print("Applying phase adjustments to converged MOs\n");
    phase_adjust(*coeffs_);
    fock_ = make_shared<qleve::Tensor<2>>(nmo_, nmo_);
    qleve::matrix_transform(1.0, fock, *coeffs_, 0.0, *fock_);
    eigs_ = make_shared<qleve::Tensor<1>>(nmo_);
    for (ptrdiff_t i = 0; i < nmo_; ++i) {
      eigs_->operator()(i) = fock_->operator()(i, i);
    }
  } else {
    throw runtime_error("Failed to converge");
  }

  if (dft_) {
    const double dens = qleve::dot_product(density_->pluck(0), molgrid_->weights());
    fmt::print("integrated density: {:f}\n", dens);
  }

  fmt::print("Computing multipole moments\n");
  timer.mark();
  auto multipole = geometry_->multipole(2);
  qleve::Tensor<1> moments(multipole.extent(2));
  qleve::contract(-2.0, multipole, "uvp", *rho_, "uv", 0.0, moments, "p");
  timer.tick_print("multipole moments");

  auto nuclear_moments = geometry_->nuclear_multipole(2);
  auto total_moments = nuclear_moments.clone();
  total_moments += moments;

  fmt::print("Electronic Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", moments(1), moments(2),
             moments(3));
  fmt::print("   Nuclear Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", nuclear_moments(1),
             nuclear_moments(2), nuclear_moments(3));
  fmt::print("     Total Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", total_moments(1),
             total_moments(2), total_moments(3));
  fmt::print("\n");
  fmt::print("Electronic Quadrupole (xx, xy, xz, yy, yz, zz): {:18.8f} {:18.8f} {:18.8f} "
             "{:18.8f} {:18.8f} {:18.8f}\n",
             moments(4), moments(5), moments(6), moments(7), moments(8), moments(9));
  fmt::print("\n");

  auto grad = this->gradient();
  timer.tick_print("gradients");

  qleve::Tensor<2> charge_density(2.0 * *rho_);
  properties::properties(input_, geometry_, charge_density);

  timer.summarize();
}

bool SCF::converged(const double dE, const double error, const double drho) const
{
  bool conv = (abs(dE) < conv_energy_) && (abs(error) < conv_error_) && (abs(drho) < conv_density_);
  return conv;
}

shared_ptr<Wfn> SCF::wavefunction()
{
  auto out = make_shared<SingleReference>(geometry_, coeffs_, fock_, nocc_, nmo_ - nocc_, energy_);
  if (dft_) {
    out->density() = density_;
    out->dft() = dft_;
  }
  return out;
}

