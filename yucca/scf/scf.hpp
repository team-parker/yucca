// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/scf/scf.hpp
///
/// SCF driver class
#pragma once

#include <memory>

#include <yucca/scf/fock.hpp>
#include <yucca/util/method.hpp>

namespace yucca
{

// use forward declarations here to prevent compilation cascades
class Geometry;          // #include <yucca/structure/geometry.hpp>
class Wfn;               // #include <yucca/wfn/wfn.hpp>
class RHFConverger;      // #include <yucca/scf/converger.hpp>
class DensityFunctional; // #include <yucca/dft/density_functional.hpp>
class MolecularGrid;     // #include <yucca/dft/molecular_grid.hpp>

class SCF : public Method_ {
 protected:
  std::ptrdiff_t nmo_;
  std::ptrdiff_t nao_;
  std::ptrdiff_t nocc_;
  std::ptrdiff_t nelec_;
  int charge_;

  // density functional
  std::shared_ptr<DensityFunctional> dft_;
  std::shared_ptr<MolecularGrid> molgrid_;
  std::shared_ptr<qleve::Tensor<2>> density_;
  ptrdiff_t dft_batch_;
  ptrdiff_t dft_thresh_;

  // important intermediates
  std::shared_ptr<qleve::Tensor<2>> overlap_;
  std::shared_ptr<qleve::Tensor<2>> hcore_;

  // output
  std::shared_ptr<qleve::Tensor<2>> coeffs_;
  std::shared_ptr<qleve::Tensor<2>> rho_;
  std::shared_ptr<qleve::Tensor<2>> fock_;
  std::shared_ptr<qleve::Tensor<1>> eigs_;
  double energy_;

  // numerical parameters
  int max_iter_;
  double conv_energy_;
  double conv_error_;
  double conv_density_;

  // initial guess
  std::string guess_;

  // fock build object
  std::unique_ptr<RFockBuilder> fock_builder_;

  // convergence helper
  std::unique_ptr<RHFConverger> converger_;

  bool converged(const double dE, const double error, const double drho) const;

 public:
  SCF(const InputNode& input, const std::shared_ptr<Geometry>& g);
  SCF(const InputNode& input, const std::shared_ptr<Wfn>& wfn);

  // copy constructor
  SCF(const SCF& x);

  // access functions
  std::ptrdiff_t nmo() const { return nmo_; }
  std::ptrdiff_t nao() const { return nao_; }
  std::ptrdiff_t nocc() const { return nocc_; }
  std::shared_ptr<qleve::Tensor<2>> hcore() const { return hcore_; }
  std::shared_ptr<qleve::Tensor<2>> fock() const { return fock_; }
  std::shared_ptr<qleve::Tensor<2>> coeffs() const { return coeffs_; }

  void compute() override;

  double energy() override { return energy_; }
  std::vector<double> energies() override { return std::vector<double>{energy_}; }

  std::shared_ptr<Wfn> wavefunction() override;

 protected:
  virtual void compute_gradient_impl() override;

  void compute_gradient_2e(const qleve::ConstTensorView<2>& coeffs,
                           const qleve::ConstTensorView<1>& occs,
                           const qleve::ConstTensorView<2>& P, qleve::TensorView<2> ddx);

  void compute_grad_2e_direct(const qleve::ConstTensorView<2>& P, qleve::TensorView<2> ddx);

  void compute_grad_2e_df(const qleve::ConstTensorView<2>& coeffs,
                          const qleve::ConstTensorView<1>& occs, qleve::TensorView<2> ddx);
};

} // namespace yucca
