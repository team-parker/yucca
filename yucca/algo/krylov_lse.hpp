// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/algo/krylov_lse.hpp
///
/// Utility algorithm for Krylov-space linear system of equations solver (Krylov LSE)
#pragma once

#include <memory>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <qleve/matrix_multiply.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/tensor.hpp>
#include <qleve/vector_ops.hpp>

namespace yucca
{

template <typename DataType>
class KrylovLSE {
 public:
  using MatType = qleve::Tensor_<DataType, 2>;
  using MatViewType = qleve::TensorView_<DataType, 2>;
  using ConstMatViewType = qleve::ConstTensorView_<DataType, 2>;
  using ArrayType = qleve::Tensor_<DataType, 1>;
  using ConstArrayViewType = qleve::ConstTensorView_<DataType, 1>;

 protected:
  ptrdiff_t size_;
  ptrdiff_t max_subspace_;
  ptrdiff_t ncurrent_;

  std::shared_ptr<ArrayType> diag_;

  std::unique_ptr<MatType> RHS_;           ///< Right-Hand-Side (RHS) vectors
  std::unique_ptr<ArrayType> frequencies_; ///< frequencies
  std::unique_ptr<ArrayType> rhs_norms_;   ///< norms of input RHS vectors

  std::unique_ptr<MatType> V_; ///< Basis vectors
  std::unique_ptr<MatType> W_; ///< Matrix multiplies
  std::unique_ptr<MatType> H_; ///< Subspace Hamiltonian
  std::unique_ptr<MatType> S_; ///< Subspace Overlap
  std::unique_ptr<MatType> R_; ///< Subspace RHS

 public:
  KrylovLSE(const ConstMatViewType& rhs, const ConstArrayViewType& freqs,
            std::shared_ptr<ArrayType> diag, const size_t max_subspace) :
      size_(diag->size()),
      max_subspace_(max_subspace),
      ncurrent_(0),
      diag_(diag),
      RHS_(std::make_unique<MatType>(rhs)),
      frequencies_(std::make_unique<ArrayType>(freqs)),
      rhs_norms_(std::make_unique<ArrayType>(freqs.size())),
      V_(std::make_unique<MatType>(size_, max_subspace_)),
      W_(std::make_unique<MatType>(size_, max_subspace_)),
      H_(std::make_unique<MatType>(max_subspace_, max_subspace_)),
      S_(std::make_unique<MatType>(max_subspace_, max_subspace_)),
      R_(std::make_unique<MatType>(max_subspace_, frequencies_->size()))
  {
    assert(RHS_->extent(1) == frequencies_->size());
    assert(RHS_->extent(0) == diag_->size());
    // normalize RHS on input
    qleve::contract(1.0, *RHS_, "xi", *RHS_, "xi", 0.0, *rhs_norms_, "i");
    *rhs_norms_ = qleve::sqrt(*rhs_norms_);
    for (ptrdiff_t i = 0; i < rhs_norms_->size(); ++i) {
      RHS_->pluck(i) *= 1.0 / rhs_norms_->at(i);
    }
  }

  /// Simplified Driver for solving an Ax=b equation using the KrylovLSE class
  ///
  /// H should behave like a function with the signature
  ///   void H(const ConstTensorView<DataType, 2> V, TensorView<DataType, 2> W)
  /// which multiplies the matrix of interest onto the set of vectors V, putting the results
  /// onto the set of vectors W
  template <typename Func>
  MatType solve(Func H, const size_t maxiter, const double thresh,
                const ptrdiff_t restart_every = -1, const bool recompute_mv = false,
                const bool save_best = false, const bool verbose = false)
  {
    ptrdiff_t nnew = rhs_norms_->size(); // number of new vectors being added to the krylov space
    const ptrdiff_t nfreq = nnew;

    // V0 and W0 will just hold the memory for the matrix vector products
    MatType V0(*RHS_);
    precondition(V0, std::vector<bool>(nnew, false));

    nnew = regularize_new_vectors(V0);

    MatType W0 = V0.zeros_like();

    if (verbose) {
      fmt::print("krylov: {:6s} {:20s}\n", "iter", "|residual|");
    }

    std::unique_ptr<MatType> best_V = save_best ? std::make_unique<MatType>(*RHS_) : nullptr;
    double best_residual = 1e9;

    for (size_t iter = 0; iter < maxiter; ++iter) {
      auto VV = V0.const_slice(0, nnew);
      auto WW = W0.slice(0, nnew);

      // WW = H * VV
      H(VV, WW);
      this->push_back(VV, WW);
      auto [V, residual] = this->estimate();

      nnew = 0;
      std::vector<double> resnrm(nfreq);
      std::vector<bool> conv(nfreq);

      fmt::print("krylov: {:6d} {:20.6g}\n", iter, residual.norm());
      for (size_t i = 0; i < nfreq; ++i) {
        resnrm[i] = std::sqrt(qleve::dot_product(size_, &residual(0, i), &residual(0, i)));
        conv[i] = (resnrm[i] < thresh);
      }

      if (std::all_of(conv.begin(), conv.end(), [](const bool x) { return x; })) {
        fmt::print("krylov: converged\n"); // converged!
        return V;
      }

      if (save_best && residual.norm() < best_residual) {
        *best_V = V;
        best_residual = residual.norm();
      }

      nnew = precondition(residual, conv);

      if (restart_every > 1 && iter > 0 && (iter % restart_every) == 0) {
        if (verbose) {
          fmt::print("krylov: restarting");
        }
        restart();

        if (recompute_mv) {
          if (verbose) {
            fmt::print(" and recomputing matrix-vector products");
          }
          nnew = ncurrent_;
          ncurrent_ = 0;
          V0.slice(0, nnew) = V_->slice(0, nnew);
          H(V0.slice(0, nnew), W0.slice(0, nnew));
          this->push_back(V0.slice(0, nnew), W0.slice(0, nnew));
        }
        if (verbose) {
          fmt::print("\n");
        }
      }

      nnew = regularize_new_vectors(residual.slice(0, nnew));
      // nnew vectors will be added to the krylov space in the next iteration
      V0.slice(0, nnew) = residual.slice(0, nnew);
    }

    auto [V, res] = this->estimate();
    if (res.norm() < best_residual) {
      if (verbose) {
        fmt::print("krylov: unconverged vectors have remaining residual {:3e}\n", res.norm());
      }
      return V;
    } else {
      if (verbose) {
        fmt::print("krylov: unconverged vectors have remaining residual {:3e}\n", best_residual);
      }
      return *best_V;
    }
  }

  void push_back(const qleve::ConstTensorView_<DataType, 2>& v,
                 const qleve::ConstTensorView_<DataType, 2>& w)
  {
    assert(v.extent(1) == w.extent(1) && v.extent(1) > 0);
    const size_t nst = v.extent(1);

    if (ncurrent_ + nst > max_subspace_) {
      throw std::runtime_error("too many vectors being passed to Davidson. Fix the algorithm");
    }

    // update V and W vectors
    V_->slice(ncurrent_, ncurrent_ + nst) = v;
    W_->slice(ncurrent_, ncurrent_ + nst) = w;
    ncurrent_ += nst;

    // brute force update of subspace overlap and Hamiltonian
    qleve::gemm("t", "n", 1.0, V_->slice(0, ncurrent_), V_->slice(0, ncurrent_), 0.0,
                S_->subtensor({0, 0}, {ncurrent_, ncurrent_}));
    qleve::gemm("t", "n", 1.0, W_->slice(0, ncurrent_), V_->slice(0, ncurrent_), 0.0,
                H_->subtensor({0, 0}, {ncurrent_, ncurrent_}));

    // update subspace RHS
    qleve::gemm("t", "n", 1.0, V_->slice(0, ncurrent_), *RHS_, 0.0,
                R_->subtensor({0, 0}, {ncurrent_, frequencies_->size()}));
  }

  std::tuple<MatType, MatType> estimate() noexcept
  {
    const ptrdiff_t nfreq = frequencies_->size();
    MatType vecs(H_->subtensor({0, 0}, {ncurrent_, ncurrent_}));

    // solve by diagonalizing (make this controllable?)
    auto eigs = qleve::linalg::diagonalize(vecs);

    auto rhs = qleve::gemm("t", "n", 1.0, vecs, R_->subtensor({0, 0}, {ncurrent_, nfreq}));

    for (ptrdiff_t i = 0; i < nfreq; ++i) {
      const double freq = frequencies_->at(i);
      rhs.pluck(i) /= (eigs - freq);
    }

    auto subspace_solution = qleve::gemm("n", "n", 1.0, vecs, rhs);

    auto fullvec = qleve::gemm("n", "n", 1.0, V_->slice(0, ncurrent_), subspace_solution);
    auto residual = qleve::gemm("n", "n", 1.0, W_->slice(0, ncurrent_), subspace_solution);

    for (size_t i = 0; i < nfreq; ++i) {
      residual.pluck(i) += -frequencies_->at(i) * fullvec.pluck(i) - RHS_->pluck(i);
    }

    // scale fullvec back to original RHS norm
    for (size_t i = 0; i < nfreq; ++i) {
      fullvec.pluck(i) *= rhs_norms_->at(i);
    }

    return {fullvec, residual};
  }

  ptrdiff_t precondition(MatViewType res, const std::vector<bool>& converged)
  {
    const ptrdiff_t nfreq = frequencies_->size();
    assert(res.extent(1) == converged.size());
    ptrdiff_t nnew = 0;
    for (size_t i = 0; i < nfreq; ++i) {
      if (!converged.at(i)) {
        for (size_t j = 0; j < size_; ++j) {
          double d = (*diag_)(j)-frequencies_->at(i);
          if (std::abs(d) < 1.0e-6) {
            d = std::copysign(1.0e-6, d);
          }
          res(j, nnew) = res(j, i) / d;
        }
        // normalize new vector
        const double nrm = qleve::dot_product(size_, &res(0, nnew), &res(0, nnew));
        qleve::scale(size_, 1.0 / std::sqrt(nrm), &res(0, nnew));
        nnew += 1;
      }
    }
    return nnew;
  }

  ptrdiff_t regularize_new_vectors(MatViewType v)
  {
    // orthogonalize new vectors against old ones
    if (ncurrent_ > 0) {
      MatType tmpV(ncurrent_, v.extent(1));
      qleve::gemm("t", "n", 1.0, V_->slice(0, ncurrent_), v, 0.0, tmpV);
      qleve::gemm("n", "n", -1.0, V_->slice(0, ncurrent_), tmpV, 1.0, v);
    }

    ptrdiff_t nnew = qleve::linalg::orthogonalize_vectors(v);
    return nnew;
  }

  /// Restart by collapsing down to best-guess vectors
  void restart() noexcept
  {
    // this is copied from estimate(). Should it be separated?

    const ptrdiff_t nfreq = frequencies_->size();
    MatType vecs(H_->subtensor({0, 0}, {ncurrent_, ncurrent_}));

    // solve by diagonalizing (make this controllable?)
    auto eigs = qleve::linalg::diagonalize(vecs);

    auto rhs = qleve::gemm("t", "n", 1.0, vecs, R_->subtensor({0, 0}, {ncurrent_, nfreq}));

    for (ptrdiff_t i = 0; i < nfreq; ++i) {
      const double freq = frequencies_->at(i);
      rhs.pluck(i) /= (eigs - freq);
    }

    auto subspace_solution = qleve::gemm("n", "n", 1.0, vecs, rhs);

    auto V = qleve::gemm("n", "n", 1.0, V_->slice(0, ncurrent_), subspace_solution);
    auto W = qleve::gemm("n", "n", 1.0, W_->slice(0, ncurrent_), subspace_solution);

    *V_ = 0.0;
    *W_ = 0.0;
    *S_ = 0.0;
    *H_ = 0.0;
    *R_ = 0.0;

    const ptrdiff_t nnew = V.extent(1);

    V_->slice(0, nnew) = V;
    W_->slice(0, nnew) = W;

    // update S
    qleve::gemm("t", "n", 1.0, V_->slice(0, nnew), V_->slice(0, nnew), 0.0,
                S_->subtensor({0, 0}, {nnew, nnew}));
    // update H
    qleve::gemm("t", "n", 1.0, W_->slice(0, nnew), V_->slice(0, nnew), 0.0,
                H_->subtensor({0, 0}, {nnew, nnew}));
    // update RHS
    qleve::gemm("t", "n", 1.0, V_->slice(0, nnew), *RHS_, 0.0,
                R_->subtensor({0, 0}, {nnew, frequencies_->size()}));

    ncurrent_ = regularize_new_vectors(V);
  }
};

} // namespace yucca
