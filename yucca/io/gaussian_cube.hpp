// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/io/gaussian_cube.hpp
///
/// IO utilities to write Gaussian cube files
#pragma once

#include <qleve/tensor.hpp>

namespace yucca
{
class Geometry;

namespace io
{

/// Write a cube file from a density matrix, with options given from an input node
void write_density_to_cube(const InputNode& input, const std::shared_ptr<Geometry>& geometry,
                           const qleve::ConstTensorView<2>& density, const std::string& filename = "density.cube");

/// Write a cube file with the given data
void write_cube_impl(const std::string& filename, const std::shared_ptr<Geometry> geometry,
                     const qleve::ConstTensorView<1>& origin_angstrom,
                     std::array<ptrdiff_t, 3> npts, const qleve::ConstTensorView<2>& delta_bohr,
                     const qleve::ConstTensorView<1>& data, const std::string& comment);

} // namespace io
} // namespace yucca
