// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/wfn.hpp
///
/// A brief description of the contents of this file
#pragma once


namespace yucca
{

class InputNode; // #include <yucca/input/input_node.hpp>
class Geometry;  // #include <yucca/structure/geometry.hpp>

class Wfn {
 protected:
  std::shared_ptr<yucca::Geometry> geometry_;

  Wfn(const std::shared_ptr<yucca::Geometry> geo) : geometry_(geo) {}

 public:
  std::shared_ptr<yucca::Geometry> geometry() const { return geometry_; }

  virtual ~Wfn() = default;

  virtual void save(const std::string& filename);
  virtual std::shared_ptr<Wfn> update(std::shared_ptr<yucca::Geometry> geometry) const;

  void save_geometry(InputNode& node);

 private:
  virtual std::shared_ptr<Wfn> update_impl(std::shared_ptr<yucca::Geometry> geometry) const = 0;
};

} // namespace yucca
