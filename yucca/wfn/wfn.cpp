// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/wfn.cpp
///
/// Implement wfn::save
#include <fstream>

#include <yucca/input/input_node.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/wfn/wfn.hpp>

using namespace yucca;
using namespace std;

void yucca::Wfn::save_geometry(InputNode& node)
{
  InputNode geonode;

  InputNode atomsnode;
  for (auto& atom : geometry_->atoms()) {
    InputNode anode;
    anode.put("symbol", atom.symbol());
    anode.put("charge", atom.charge());
    anode.put("mass", atom.mass());
    anode.put("position", atom.position());

    atomsnode.push_back(anode);
  }
  geonode.put("atoms", atomsnode);
  if (!geometry_->electric_field().empty()) {
    geonode.put("electric field", geometry_->electric_field());
  }

  node.put("molecule", geonode);
}

shared_ptr<Wfn> yucca::Wfn::update(const shared_ptr<Geometry> geometry) const
{
  // TODO const bool same_atoms = this->geometry_->atoms() == geometry->atoms();
  const bool same_atoms = true;
  if (!same_atoms) {
    return nullptr;
  }

  const bool same_basis = this->geometry_->orbital_basis() == geometry->orbital_basis();
  const bool same_structure = true;
  if (!(same_basis && same_structure)) {
    // TODO : implement basis change
    return nullptr;
  }

  // if nothing else changed, clone and replace geometry
  return update_impl(geometry);
}

void yucca::Wfn::save(const std::string& filename)
{
  InputNode out;

  save_geometry(out);

  std::ofstream outstream(filename);

  out.print(outstream);
}
