// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/single_reference.cpp
///
/// Wavefunction describing a single reference wavefunction

#include <fstream>

#include <yucca/input/input_node.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/wfn/single_reference.hpp>

using namespace yucca;
using namespace std;

yucca::SingleReference::SingleReference(const shared_ptr<yucca::Geometry> geom,
                                        const shared_ptr<qleve::Tensor<2>> cc,
                                        const shared_ptr<qleve::Tensor<2>> fock,
                                        const ptrdiff_t nocc, const ptrdiff_t nvir,
                                        const double energy) :
    Wfn(geom),
    coeffs_(cc),
    fock_(fock),
    nmo_(cc->extent(1)),
    nocc_(nocc),
    nvir_(nvir),
    charge_(geom->total_nuclear_charge() - 2 * nocc),
    nspin_(1),
    spin_(0),
    energy_(energy)
{}

void yucca::SingleReference::save(const std::string& filename)
{

  InputNode outnode;

  this->save_geometry(outnode);

  outnode.put("type", "single reference");
  outnode.put("nmo", nmo_);
  outnode.put("nocc", nocc_);
  outnode.put("nvir", nvir_);
  outnode.put("energy", energy_);

  outnode.put("orbitals", *coeffs_);
  outnode.put("fock", *fock_);

  std::ofstream outstream(filename);
  outnode.print(outstream);
}

shared_ptr<Wfn> yucca::SingleReference::update_impl(shared_ptr<Geometry> geometry) const
{
  auto out = make_shared<SingleReference>(geometry, coeffs_, fock_, nocc_, nvir_, energy_);
  out->dft_ = dft_;

  return out;
}
