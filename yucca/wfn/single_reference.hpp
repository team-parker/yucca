// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/wfn/single_reference.hpp
///
/// Wavefunction describing a single reference wavefunction

#pragma once

#include <yucca/wfn/wfn.hpp>

namespace yucca
{

class DensityFunctional; // fwd decl

class SingleReference : public Wfn {
 protected:
  std::shared_ptr<qleve::Tensor<2>> coeffs_;
  std::shared_ptr<qleve::Tensor<2>> fock_;
  std::shared_ptr<qleve::Tensor<2>> density_;
  std::shared_ptr<yucca::DensityFunctional> dft_;

  ptrdiff_t nmo_;
  ptrdiff_t nocc_;
  ptrdiff_t nvir_;

  ptrdiff_t charge_;
  ptrdiff_t spin_;  // Nalpha - Nbeta
  ptrdiff_t nspin_; // number of spins present (i.e., 1 for RHF, 2 for UHF) TODO rename

  double energy_;

 public:
  SingleReference(const std::shared_ptr<yucca::Geometry> geom,
                  const std::shared_ptr<qleve::Tensor<2>> cc,
                  const std::shared_ptr<qleve::Tensor<2>> fock, const ptrdiff_t nocc,
                  const ptrdiff_t nvir, const double energy);

  std::shared_ptr<qleve::Tensor<2>>& coeffs() { return coeffs_; }
  const std::shared_ptr<qleve::Tensor<2>>& coeffs() const { return coeffs_; }

  std::shared_ptr<qleve::Tensor<2>>& fock() { return fock_; }
  const std::shared_ptr<qleve::Tensor<2>>& fock() const { return fock_; }

  std::shared_ptr<qleve::Tensor<2>>& density() { return density_; }
  const std::shared_ptr<qleve::Tensor<2>>& density() const { return density_; }

  std::shared_ptr<yucca::DensityFunctional>& dft() { return dft_; }
  const std::shared_ptr<yucca::DensityFunctional>& dft() const { return dft_; }

  ptrdiff_t nmo() const { return nmo_; }
  ptrdiff_t nocc() const { return nocc_; }
  ptrdiff_t nvir() const { return nvir_; }

  ptrdiff_t charge() const { return charge_; }
  ptrdiff_t spin() const { return spin_; }

  ptrdiff_t nspin() const { return nspin_; }

  double energy() const { return energy_; }

  virtual void save(const std::string& filename) override;

 private:
  virtual std::shared_ptr<Wfn> update_impl(std::shared_ptr<Geometry> geometry) const override;
};

} // namespace yucca
