// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/ureshf.cpp
///
/// uResHF driver class

#include <fmt/core.h>

#include <qleve/checks.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/rotate.hpp>
#include <qleve/trace.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/io/gaussian_cube.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/resmf/ureshf.hpp>
#include <yucca/structure/basis.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/wfn/single_reference.hpp>
#include <yucca/wfn/ureshf_reference.hpp>

using namespace std;
using namespace yucca;
using namespace qleve;

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Method Constructors                                             //
///////////////////////////////////////////////////////////////////////////////////////

yucca::uResHF::uResHF(const InputNode& input, const shared_ptr<Geometry>& g) : Method_(input, g)
{
  fmt::print("Setting up uResHF calculation\n");

  // set up wavefunction optimization cutoffs
  max_iter_ = input.get<ptrdiff_t>("max_iter", 30);

  // this is an important block: set it up rigorously externally somewhere
  string conv_key = input.get_if_value<string>("convergence", "default");
  if (conv_key == "loose") {
    conv_energy_ = 1e-6;
    conv_error_ = 1e-3;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "default") {
    conv_energy_ = 1e-7;
    conv_error_ = 1e-5;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "tight") {
    conv_energy_ = 1e-8;
    conv_error_ = 1e-6;
    conv_density_ = 1e10; // TODO
  } else if (conv_key == "ultratight") {
    conv_energy_ = 1e-9;
    conv_error_ = 1e-7;
    conv_density_ = 1e10; // TODO
  } else {
    throw runtime_error("Unknown convergence option: use loose, default or tight");
  }

  // allow specific overrides
  if (auto conv = input.get_node_if("convergence"); conv && conv->is_map()) {
    conv_energy_ = conv->get<double>("energy", conv_energy_);
    conv_error_ = conv->get<double>("grad", conv_error_);
    conv_density_ = conv->get<double>("density", conv_density_);
  }

  fmt::print("Convergence criteria:\n");
  fmt::print("  - change in energy (dE) < {:1e}\n", conv_energy_);
  fmt::print("  - gradient norm (grad) < {:1e}\n", conv_error_);
  fmt::print("  - change in density (drho) < {:1e}\n", conv_density_);
  fmt::print("\n");

  // set up AO basis quantities
  nao_ = geometry_->orbital_basis()->nbasis();
  if (!hcore_)
    hcore_ = make_shared<qleve::Tensor<2>>(geometry_->hcore());
  if (!overlap_)
    overlap_ = make_shared<qleve::Tensor<2>>(geometry_->overlap());

  // set up uResHF method options
  svd_floor_ = input.get<double>("svd_floor", 0.0);
  fmt::print("Suppressing singular values to have a floor of {:3e}\n", svd_floor_);

  use_matadj_ = input.get<bool>("matrix adjugate", true);
  if (use_matadj_) {
    fmt::print("Using matrix adjugate uResHF Fock build implementation!\n");
  }

  print_kab_ = input.get<bool>("print kab", false);
  if (print_kab_) {
    fmt::print("Fock matrix debug mode - Printing KAB terms enabled!\n");
  }

  fmt::print("\n");

  // Not real happy with this current code flow...
  // There's a "silent error" when you forget to specify the SCF
  // precursor stage... Do we really need both of these constructors?
  // Should we look into making (u)ResHF able to start without precursor calcs? - ERM

} // end first uResHF constructor

yucca::uResHF::uResHF(const InputNode& input, const shared_ptr<Wfn>& wfn) :
    uResHF(input, wfn->geometry())
{
  fmt::print("Pulling in starting orbitals for uResHF wavefunction initialization:\n\n");

  // dynamic cast of input wavefunction to a restricted SCF wavefunction
  const shared_ptr<SingleReference> rscf_ref = dynamic_pointer_cast<SingleReference>(wfn);
  const shared_ptr<uResHFReference> ureshf_ref = dynamic_pointer_cast<uResHFReference>(wfn);

  // initialize class variables using previously run restricted SCF calculation
  if (rscf_ref) {
    // set up molecular orbitals from a prior restricted HF calculation
    rscf_coeffs_ = make_shared<qleve::Tensor<2>>(*rscf_ref->coeffs());
    assert(rscf_coeffs_);

    nmo_ = rscf_coeffs_->extent(1);
    nelec_ = 2 * rscf_ref->nocc();
    charge_ = rscf_ref->geometry()->total_nuclear_charge() - nelec_;

    fmt::print("Preliminary restricted SCF reference properties:\n");
    fmt::print("  - nmo:    {}\n", nmo_);
    fmt::print("  - nelec:  {}\n", nelec_);
    fmt::print("  - charge: {}\n", charge_);
    fmt::print("\n");

    multiplicity_ = input.get<string>("multiplicity", "singlet");
    fmt::print("User requested uResHF wavefunction of {} spin multiplicity.\n\n", multiplicity_);
    if (multiplicity_ == "singlet") {

      nel_alpha_ = nelec_ / 2;
      nel_beta_ = nel_alpha_;

      nvir_alpha_ = nmo_ - nel_alpha_;
      nvir_beta_ = nvir_alpha_;

    } else if (multiplicity_ == "triplet") {

      nel_alpha_ = nelec_ / 2;
      nel_beta_ = nel_alpha_;
      nel_alpha_ += 1;
      nel_beta_ -= 1;
      assert(nel_alpha_ == (nel_beta_ + 2));

      nvir_alpha_ = nmo_ - nel_alpha_;
      nvir_beta_ = nmo_ - nel_beta_;

    } else {
      throw runtime_error("Unrecognized spin multiplicity! Currently only able to accomodate "
                          "singlet and triplet uResHF wavefunctions.");
    }

    fmt::print("Casting restricted MOs into unrestricted MO basis:\n");
    fmt::print("For alpha-spin electrons:\n");
    fmt::print("  - alpha-spin nmo:   {}\n", nmo_);
    fmt::print("  - alpha-spin nelec: {}\n", nel_alpha_);
    fmt::print("  - alpha-spin nvir:  {}\n", nvir_alpha_);

    fmt::print("For beta-spin electrons:\n");
    fmt::print("  - beta-spin nmo:   {}\n", nmo_);
    fmt::print("  - beta-spin nelec: {}\n", nel_beta_);
    fmt::print("  - beta-spin nvir:  {}\n", nvir_beta_);
    fmt::print("\n");

    // set up initial Slater determinants from user-input
    fmt::print("User-specified initial Slater determinants:\n");

    nSD_ = 0;
    nstates_ = 0;
    nrotSD_ = 0;

    // initialize holders for orbital rotation initial guess
    bool rotate_requested = false;
    vector<vector<vector<ptrdiff_t>>> hold_rot_orbitals{};
    vector<vector<double>> hold_rot_angles{};
    vector<vector<string>> hold_rot_spin{};

    auto guessnode = input.get_node("initial guess");
    // recast initial guess node as vector to allow for iteration through
    auto guessvec = get<vector<recursive_wrapper<InputNode>>>(guessnode.data_);
    for (auto& element : guessvec) {

      // pull out the "type" of initial guess and store as key variable
      const InputNode& typenode = element.get();
      string key = typenode.get_if_value<string>("type", "default");

      // save key for later to use when actually building initial guess determinants
      guess_types_.push_back(key);

      if (key == "hf" || key == "default") {
        fmt::print("  - Slater determinant #{}: HF ground state\n", nSD_);

        // update counters to track determinants and uResHF states
        nSD_++;
        nstates_++;
      } else if (key == "rotate") {
        fmt::print("  - Slater determinant #{}: Orbital rotations of HF ground state\n", nSD_);
        rotate_requested = true;

        // pull out "orbital:" node containing vectors in each element
        InputNode orbitnode;
        if (auto orbitcheck = typenode.get_node_if("orbitals"); orbitcheck) {
          orbitnode = typenode.get_node("orbitals");
        } else {
          throw runtime_error("No rotation orbitals defined! 'orbitals:' key is missing");
        }

        // initialize holder for pairs of orbitals to be rotated
        vector<vector<ptrdiff_t>> orbitpair{};

        // extract and validate each user-specified orbital pair (- [i,a]) in "orbitals:"
        for (ptrdiff_t ipair = 0; ipair < orbitnode.size(); ipair++) {

          // access and store orbital pair vector (- [i,a])
          auto pairvec = orbitnode.get_node(ipair);
          orbitpair.push_back(pairvec.as_vector<ptrdiff_t>());

          // check to make sure the user didn't put in anything weird
          for (auto& mo : pairvec.as_vector<ptrdiff_t>()) {
            if (mo < 0) {
              throw runtime_error("Orbital index values must be greater than or equal to 0!");
            }

            if (mo > nmo_) {
              throw runtime_error("Requested orbital index exceeds available MOs!");
            }
            // NOTE: I think this is robust enough? Because there's no real reason for the TOTAL
            // number of beta orbitals to be different from alpha orbitals, even if the number of
            // OCCUPIED orbitals might be different between alpha and beta. - ER

          } // to next MO index

        } // to next orbital pair

        // save all requested orbital rotation pairs for this determinant
        hold_rot_orbitals.push_back(orbitpair); // will be reorganized later!


        // access and store rotation angles for each orbital rotation in this determinant
        if (auto anglenode = typenode.get_node_if("angles"); anglenode) {
          if (anglenode->is_vector()) {
            hold_rot_angles.push_back(
                typenode.get_vector<double>("angles")); // will be reorganized later!
          }
        } else {
          throw runtime_error("No rotation angles defined! 'angles:' key is missing");
        }

        // access and store spin labels for each orbital rotation in this determinant
        if (auto spinnode = typenode.get_node_if("spin"); spinnode) {
          if (spinnode->is_vector()) {
            hold_rot_spin.push_back(
                typenode.get_vector<string>("spin")); // will be reorganized later!
          }
        } else {
          throw runtime_error("No orbital spin specified! 'spin:' key is missing");
        }

        // sanity check that the user gave the correct amounts of entries for everything
        for (ptrdiff_t idet = 0; idet < hold_rot_orbitals.size(); idet++) {
          if (hold_rot_orbitals[idet].size() != hold_rot_angles[idet].size()
              || hold_rot_orbitals[idet].size() != hold_rot_spin[idet].size()
              || hold_rot_spin[idet].size() != hold_rot_angles[idet].size()) {
            throw runtime_error("Must provide same number of orbital pairs, rotation angles, and "
                                "spin identifiers for a "
                                "given determinant!");
          }
        }

        // update counters to track determinants and uResHF states
        nSD_++;
        nstates_++;
        nrotSD_++;

      } else if (key == "cis") {
        throw runtime_error(
            "CIS initial guess not yet implemented for uResHF! Pro-tip: you can probably do what "
            "you were hoping to by using the orbital rotation initial guess!");
      } else {
        throw runtime_error("Unknown initial guess input specified in 'type:' key!");
      }
    } // move to next user-input initial guess type

    // reorganizing orbital rotation pairs, angles, and spin labels into a single vector holder
    // (rot_pairs_), which will be used later to build initial determinants
    if (rotate_requested) {
      for (ptrdiff_t idet = 0; idet < hold_rot_angles.size(); idet++) {
        auto orbitvec = hold_rot_orbitals[idet];
        auto anglevec = hold_rot_angles[idet];
        auto spinvec = hold_rot_spin[idet];
        vector<tuple<ptrdiff_t, ptrdiff_t, double, string>> detvec{};

        // iterate through all orbital rotations requested for a given determinant
        for (ptrdiff_t irot = 0; irot < orbitvec.size(); irot++) {
          auto orbit_pair = orbitvec[irot];
          auto angle = anglevec[irot];
          auto spin = spinvec[irot];

          auto rot_set = std::make_tuple(orbit_pair[0], orbit_pair[1], angle, spin);

          detvec.push_back(rot_set);
        }
        rot_pairs_.push_back(detvec);
      } // to the next determinant
    }

    fmt::print("\n");

  } else if (ureshf_ref) {
    fmt::print("Initial guess generated from previous uResHF calculation\n");

    rscf_coeffs_ = ureshf_ref->scf();
    sdets_ = ureshf_ref->sdets();

    nmo_ = ureshf_ref->nmo();
    auto [nea, neb] = ureshf_ref->nelec();
    nel_alpha_ = nea;
    nel_beta_ = neb;
    nvir_alpha_ = nmo_ - nel_alpha_;
    nvir_beta_ = nmo_ - nel_beta_;

    nelec_ = nel_alpha_ + nel_beta_;
    charge_ = ureshf_ref->geometry()->total_nuclear_charge() - nelec_;

    auto wfnco = ureshf_ref->wfn_coeffs();
    nSD_ = wfnco->extent(0);
    nstates_ = wfnco->extent(1);

  } else {
    throw runtime_error("Uh oh, I need some starting MOs to work with! Please run an SCF "
                        "or uResHF calculation prior to running uResHF!");
  } // end prior calculation if/else conditional

  fmt::print("State averaging set up:\n");

  // check if user has specified the "weights:" node for state averaging,
  // and if not, then throw an error (no default set!)
  if (auto weightnode = input.get_node_if("weights"); weightnode) {
    // check if user provided a vector, and if not, throw error
    if (weightnode->is_vector()) {

      // access and validate state averaging weights
      state_weight_ = input.get_vector<double>("weights");
      double weight_sum = 0.0; // tracker for weight normalization
      for (ptrdiff_t i = 0; i < state_weight_.size(); i++) {
        if (state_weight_[i] > 0.0) {
          fmt::print("  - State {} selected for averaging with weight {}\n", i, state_weight_[i]);
        }
        if (state_weight_[i] < 0.0) {
          throw runtime_error("Weights to be applied to states must be non-negative!");
        }
        weight_sum += state_weight_[i];
      }
      fmt::print("\n");

      // normalize state averaging weights to 1.0 if needed
      if (weight_sum != 1.00) {
        fmt::print("Normalizing weights to 1.0\n");
        fmt::print("New weights: ");
        for (ptrdiff_t i = 0; i < state_weight_.size(); ++i) {
          state_weight_[i] /= weight_sum;
          fmt::print("{:12.6f}", state_weight_[i]);
        }
        fmt::print("\n");
      }
    } else {
      throw runtime_error("'weights:' block incorrectly formatted! Please format as vector, such "
                          "as 'weights: [1.0, 1.0]'");
    }
  } else {
    throw runtime_error(
        "Please specify State Averaging with 'weights:' block. If single-state "
        "ResHF is desired, use 'weights: [1.0]' for ground SS-ResHF, or 'weights: [0.0, "
        "1.0]' for S1 state SS-ResHF, etc.");
  }

  assert(nSD_ == nstates_); // might need to change this later if we end up doing tricks like in
                            // restricted ResHF

  if (state_weight_.size() > nstates_) {
    throw runtime_error("Number of states to be averaged must be less than or equal to number of "
                        "total ResHF states (number of determinants)!");
  }

  fmt::print("\n");

  // initialize converger (uconverger)
  std::array<ptrdiff_t, 2> nelec = {nel_alpha_, nel_beta_};
  converger_ = make_unique<UResHFConverger>(input, nelec);

} // end second uResHF constructor

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Debug Copy Constructor                                          //
///////////////////////////////////////////////////////////////////////////////////////

yucca::uResHF::uResHF(const uResHF& x) : Method_(x)
{
  this->nao_ = x.nao_;
  this->nmo_ = x.nmo_;

  this->overlap_ = make_shared<Tensor<2>>(*x.overlap_);
  this->hcore_ = make_shared<Tensor<2>>(*x.hcore_);

  this->rscf_coeffs_ = make_shared<Tensor<2>>(*x.rscf_coeffs_);
  this->nelec_ = x.nelec_;
  this->charge_ = x.charge_;

  this->multiplicity_ = x.multiplicity_;

  this->nel_alpha_ = x.nel_alpha_;
  this->nel_beta_ = x.nel_beta_;
  this->nvir_alpha_ = x.nvir_alpha_;
  this->nvir_beta_ = x.nvir_beta_;

  this->nSD_ = x.nSD_;
  this->nstates_ = x.nstates_;

  this->state_weight_ = x.state_weight_;

  this->sdets_ = make_shared<Tensor<4>>(*x.sdets_);

  this->guess_types_ = x.guess_types_;

  this->nrotSD_ = x.nrotSD_;
  this->rot_pairs_ = x.rot_pairs_;

  this->uresfockbuilder_ = make_shared<uResFockBuilder>(*x.uresfockbuilder_);
  this->print_kab_ = x.print_kab_;
  this->use_matadj_ = x.use_matadj_;
  this->svd_floor_ = x.svd_floor_;

  this->energies_ = make_shared<Tensor<1>>(*x.energies_);
  this->resHF_coeffs_ = make_shared<Tensor<2>>(*x.resHF_coeffs_);
  this->energySA_ = x.energySA_;

  this->conv_energy_ = x.conv_energy_;
  this->conv_error_ = x.conv_error_;
  this->conv_density_ = x.conv_density_;

  this->max_iter_ = x.max_iter_;

  // skipping copying of converger_alpha_ and converger_beta_ because unique pointers get weird...

  this->orb_gradient_norm_ = x.orb_gradient_norm_;

} // end uResHF copy constructor

///////////////////////////////////////////////////////////////////////////////////////
//      Begin uResHF Compute Function                                                //
///////////////////////////////////////////////////////////////////////////////////////

void yucca::uResHF::compute()
{
  fmt::print("Starting Unrestricted ResHF calculation\n\n");

  if (!sdets_) {
    fmt::print("Building initial uResHF wavefucntion from user-specified determinants:\n");

    // initialize holders for Slater determinants
    sdets_ = make_shared<Tensor<4>>(nao_, nmo_, nSD_, 2);

    // set Slater determinant counters to zero
    ptrdiff_t det = 0;  // counter for all Slater determinants
    ptrdiff_t rdet = 0; // counter for orbital rotation Slater determinants

    // begin iterating through all user-specified determinant initial guesses
    for (auto& key : guess_types_) {
      if (key == "hf" || key == "default") {
        fmt::print("  - Storing ground state HF as Slater determinant #{}\n", det);

        // initialize alpha and beta orbitals from the same set of restriced HF orbitals,
        // and store MOs directly with no further modification

        auto sdview_alpha = sdets_->pluck(det, 0);
        assert(sdview_alpha.extent(1) == rscf_coeffs_->extent(1));
        sdview_alpha = *rscf_coeffs_;

        auto sdview_beta = sdets_->pluck(det, 1);
        assert(sdview_beta.extent(1) == rscf_coeffs_->extent(1));
        sdview_beta = *rscf_coeffs_;

        det++;

      } else if (key == "rotate") {
        fmt::print("  - Storing orbital rotated HF determinant as Slater determinant #{}\n", det);

        // access the determinant placeholder from class variables

        auto sdview_alpha = sdets_->pluck(det, 0);
        assert(sdview_alpha.extent(1) == rscf_coeffs_->extent(1));

        auto sdview_beta = sdets_->pluck(det, 1);
        assert(sdview_beta.extent(1) == rscf_coeffs_->extent(1));

        // access information about orbitals to be rotated
        auto rotvec = rot_pairs_[rdet];

        // initialize alpha and beta orbitals from the same set of restriced HF orbitals
        sdview_alpha = *rscf_coeffs_;
        sdview_beta = *rscf_coeffs_;

        // iterate through each orbital rotation requested for this determinant
        for (ptrdiff_t irot = 0; irot < rotvec.size(); irot++) {
          auto [i, a, theta, omega] = rotvec[irot];

          double theta_rad = (theta / 180) * physical::pi<double>;

          // determine whether to rotate alpha or beta orbitals
          if (omega == "alpha") {
            // print info about which orbitals will be rotated
            fmt::print("     * Rotating alpha-spin orbitals:\n");
            if (i < nel_alpha_) {
              fmt::print("       occupied orbital {}\n", i);
            } else {
              fmt::print("       virtual orbital {}\n", i);
            }
            if (a < nel_alpha_) {
              fmt::print("       occupied orbital {}\n", a);
            } else {
              fmt::print("       virtual orbital {}\n", a);
            }
            fmt::print("       rotation angle {}\n", theta);

            // perform orbital rotation
            rotate(nao_, &sdview_alpha(0, i), 1, &sdview_alpha(0, a), 1, cos(theta_rad),
                   sin(theta_rad));

          } else if (omega == "beta") {
            // print info about which orbitals will be rotated
            fmt::print("     * Rotating beta-spin orbitals:\n");
            if (i < nel_beta_) {
              fmt::print("       occupied orbital {}\n", i);
            } else {
              fmt::print("       virtual orbital {}\n", i);
            }
            if (a < nel_beta_) {
              fmt::print("       occupied orbital {}\n", a);
            } else {
              fmt::print("       virtual orbital {}\n", a);
            }
            fmt::print("       rotation angle {}\n", theta);

            // perform orbital rotation
            rotate(nao_, &sdview_beta(0, i), 1, &sdview_beta(0, a), 1, cos(theta_rad),
                   sin(theta_rad));

          } else {
            throw runtime_error("Unknown spin identifier! Please input either alpha or beta!");
            // hopefully you'll never trigger this, as this should be caught during input parsing
          }
        }

        det++;
        rdet++;

      } else if (key == "cis") {
        throw runtime_error("Not yet implemented!");
      } else {
        throw runtime_error("Unknown initial guess type!");
      }
    } // to next guess type key
  } // end conditional initial guess generation


  // make sure orbitals are orthogonal within each set
  fmt::print("Checking orthogonality of MOs within each Slater determinant:\n");
  bool any_nonorthogonal = false;

  fmt::print("  - Alpha Spin MO's:\n");
  Tensor<2> olap_a(nmo_, nmo_);
  for (ptrdiff_t sd = 0; sd < nSD_; ++sd) {
    auto cc_a = sdets_->pluck(sd, 0);
    matrix_transform(1.0, *overlap_, cc_a, 0.0, olap_a);
    const double id = linalg::ident(olap_a);
    fmt::print("     * SD{} CSC ident? = {}\n", sd, id);
    if (id > 1e-12) {
      any_nonorthogonal = true;
      auto u_a = linalg::orthogonalize_metric(olap_a, 1e-12);
      auto ccnew_a = gemm("n", "n", 1.0, cc_a, u_a);
      cc_a = ccnew_a;
    }
  }

  fmt::print("  - Beta Spin MO's:\n");
  Tensor<2> olap_b(nmo_, nmo_);
  for (ptrdiff_t sd = 0; sd < nSD_; ++sd) {
    auto cc_b = sdets_->pluck(sd, 1);
    matrix_transform(1.0, *overlap_, cc_b, 0.0, olap_b);
    const double id = linalg::ident(olap_b);
    fmt::print("     * SD{} CSC ident? = {}\n", sd, id);
    if (id > 1e-12) {
      any_nonorthogonal = true;
      auto u_b = linalg::orthogonalize_metric(olap_b, 1e-12);
      auto ccnew_b = gemm("n", "n", 1.0, cc_b, u_b);
      cc_b = ccnew_b;
    }
  }

  if (any_nonorthogonal) {
    fmt::print("Input orbitals found to be nonorthogonal. Orthogonalizing.\n\n");
  } else {
    fmt::print("Input orbitals are good to go!\n\n");
  }

  // initialize uResHF fock builder
  uresfockbuilder_ = make_shared<uResFockBuilder>(
      *geometry_, nel_alpha_, nel_beta_, nmo_, state_weight_, svd_floor_, use_matadj_, print_kab_);

  // initialize key holders for information (to be printed after optimization)
  energies_ = make_shared<Tensor<1>>(nstates_);
  resHF_coeffs_ = make_shared<Tensor<2>>(nSD_, nstates_);
  fock_ = make_shared<Tensor<4>>(nao_, nao_, nSD_, 2);

  // set variables to control wavefunc. opt. convergence and printing
  energySA_ = 0.0;
  double nuclear_repulsion = geometry_->nuclear_repulsion();
  double old_energy = 0.0;
  bool conv = false;

  fmt::print("Starting wavefunction optimization\n");
  fmt::print("Note: defaults to use of SCF converger unless otherwise stated\n\n");

  fmt::print("{:>17s} {:>14s} {:>14s} {:>8s} {:>8s} {:>8s} {:>10s} \n", "iter", "E0 E", "E_SA",
             "|gradient|", "det-cond", "orb-cond", "dE");
  fmt::print("{:-^93s}\n", "");

  // begin wavefunction optimization iterations
  for (ptrdiff_t iopt = 0; iopt < max_iter_; iopt++) {

    // build alpha and beta fock matrices from current iteration of Slater determinants
    *fock_ = (*uresfockbuilder_)(sdets_->pluck(0), sdets_->pluck(1));

    // access some important intermediates created during fock building
    energies_ = uresfockbuilder_->energies();
    energySA_ = uresfockbuilder_->ESA() + nuclear_repulsion;
    resHF_coeffs_ = uresfockbuilder_->coeffs();
    HAB_ = uresfockbuilder_->hab();
    gammaAB_ = uresfockbuilder_->densities();
    detSAB_ = uresfockbuilder_->det_overlap();
    eta_ = uresfockbuilder_->eta();
    detsig_ = uresfockbuilder_->det_sigma();
    auto fockerr = *(uresfockbuilder_->error());

    // update convergers with status info about this optimization iteration
    converger_->push(*fock_, fockerr, energySA_);

    // get some key results ready for printing
    double a_orb_grad_norm = fockerr.const_pluck(0).norm();
    double b_orb_grad_norm = fockerr.const_pluck(1).norm();

    orb_gradient_norm_ = (a_orb_grad_norm + b_orb_grad_norm) / 2.0;

    double dE = 0.0;
    if (iopt != 0) {
      dE = energySA_ - old_energy;
    } else {
      dE = 1000.00; // just a funny number to prevent converger from thinking 1 iteration is "done"
    }
    old_energy = energySA_;

    // print said results
    fmt::print("{:s} {:6d} {:14.8f} {:14.8f} {:8.2g} {:8.2g} {:8.2g}", "uResHF iter:", iopt,
               energies_->at(0) + nuclear_repulsion, energySA_, orb_gradient_norm_, 0.0, 42.0);

    if (iopt != 0) {
      fmt::print("{:10.2g}", dE);
    } else {
      fmt::print("{:>8s}", "---");
    }
    fmt::print("\n");

    // check wavefunction convergence
    conv = converged(dE, a_orb_grad_norm, b_orb_grad_norm, 0.0);
    if (conv) {
      fmt::print("uResHF Variational Optimization has converged! :D \n\n");
      break;
    }

    // update Slater determinants (if not final iteration)
    if (iopt != (max_iter_ - 1)) {
      converger_->next(*sdets_);
    }
  } // onwards to the next optimization step!

  // print some properties and such
  fmt::print("How about some final results?\n\n");

  fmt::print("uResHF State (Total) Energies (hartrees): \n");
  for (ptrdiff_t ist = 0; ist < energies_->size(); ist++) {
    fmt::print("   * uResHF State #{}: {:14.8f}\n", ist, energies_->at(ist) + nuclear_repulsion);
  }
  fmt::print("\n");

  fmt::print("uResHF State (Electronic) Energies (hartrees): \n");
  for (ptrdiff_t ist = 0; ist < energies_->size(); ist++) {
    fmt::print("   * uResHF State #{}: {:14.8f}\n", ist, energies_->at(ist));
  }
  fmt::print("\n");

  fmt::print("uResHF Wavefunction Composition: \n");
  for (ptrdiff_t ist = 0; ist < resHF_coeffs_->extent(1); ist++) {
    fmt::print("   * uResHF State #{}: \n", ist);
    for (ptrdiff_t idet = 0; idet < resHF_coeffs_->extent(0); idet++)
      fmt::print("      - Slater determinant #{} contribution: {:14.8f} \n", idet,
                 resHF_coeffs_->at(idet, ist));
  }
  fmt::print("\n");

  fmt::print("Interdeterminant Hamiltonian Matrix < A | H | B > \n");
  HAB_->print("HAB");

  fmt::print("Determinant Overlap Matrix < A | B > \n");
  detSAB_->print("detSAB");

  fmt::print("Computing multipole moments\n");

  auto multipole = geometry_->multipole(2);
  qleve::Tensor<2> moments(multipole.extent(2), nstates_);
  auto densities = compute_1rdms(*gammaAB_, *resHF_coeffs_);
  qleve::contract(-1.0, multipole, "uvp", densities, "uvK", 0.0, moments, "pK");

  auto nuclear_moments = geometry_->nuclear_multipole(2);
  auto total_moments = nuclear_moments.zeros_like();

  for (int istate = 0; istate < nstates_; ++istate) {
    total_moments = nuclear_moments + moments.pluck(istate);
    fmt::print("Properties for state {:d}\n", istate);
    fmt::print("Electronic Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", moments(1, istate),
               moments(2, istate), moments(3, istate));
    fmt::print("   Nuclear Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", nuclear_moments(1),
               nuclear_moments(2), nuclear_moments(3));
    fmt::print("     Total Dipole (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", total_moments(1),
               total_moments(2), total_moments(3));
    fmt::print("\n");
    fmt::print("Electronic Quadrupole (xx, xy, xz, yy, yz, zz): {:18.8f} {:18.8f} {:18.8f} "
               "{:18.8f} {:18.8f} {:18.8f}\n",
               moments(4, istate), moments(5, istate), moments(6, istate), moments(7, istate),
               moments(8, istate), moments(9, istate));
    fmt::print("\n");
  }

  auto tdens = compute_1tdms(*gammaAB_, *resHF_coeffs_);
  qleve::Tensor<2> transition_moments(multipole.extent(2), tdens.extent(2));
  qleve::contract(-1.0, multipole, "uvp", tdens, "uvK", 0.0, transition_moments, "pK");

  for (int jstate = 0, ij = 0; jstate < nstates_; ++jstate) {
    for (int istate = 0; istate < jstate; ++istate, ++ij) {
      fmt::print("Transition Properties for <{:d}|V|{:d}>\n", istate, jstate);
      const double x = transition_moments(1, ij);
      const double y = transition_moments(2, ij);
      const double z = transition_moments(3, ij);
      fmt::print("<{:d}|µ|{:d}> (x, y, z): {:18.8f} {:18.8f} {:18.8f}\n", istate, jstate, x, y, z);

      const double dipnorm = std::sqrt(x * x + y * y + z * z);
      const double omega = energies_->at(jstate) - energies_->at(istate);
      const double fosc = dipnorm * dipnorm * 2.0 / 3.0 * omega;
      fmt::print("|<{0:d}|µ|{1:d}>| = {2:18.8f}, f_{0:d},{1:d} = {3:18.8f}\n", istate, jstate,
                 dipnorm, fosc);

      fmt::print("\n");
    }
  }

  fmt::print("Compute SA 1RDM and properties\n");
  // use HF coeffs as base orthogonal orbitals
  auto SC = qleve::gemm("n", "n", 1.0, *overlap_, *rscf_coeffs_);
  qleve::Tensor<2> gamma_sa(nao_, nao_);
  qleve::Tensor<1> w(nstates_);
  std::copy_n(state_weight_.data(), state_weight_.size(), w.data());
  qleve::contract(1.0, densities, "uvk", w, "k", 0.0, gamma_sa, "uv");

  qleve::Tensor<2> gamma_mo(nmo_, nmo_);
  // minus one factor so largest values come first
  qleve::matrix_transform(-1.0, gamma_sa, SC, 0.0, gamma_mo);
  auto occs = qleve::linalg::diagonalize(gamma_mo);
  occs *= -1.0;
  for (ptrdiff_t i = 0; i < occs.size(); ++i) {
    if (occs(i) < 1e-6) {
      break;
    }
    fmt::print("{:6d} {:10.6f}\n", i, occs(i));
  }

  fmt::print("Computing <S^2> for states\n");

  qleve::Tensor<3> Cocc_alpha(nao_, nel_alpha_, nSD_);
  qleve::Tensor<3> Cocc_beta(nao_, nel_beta_, nSD_);
  for (ptrdiff_t sd = 0; sd < nSD_; sd++) {
    Cocc_alpha.pluck(sd) = sdets_->const_pluck(sd, 0).const_slice(0, nel_alpha_);
    Cocc_beta.pluck(sd) = sdets_->const_pluck(sd, 1).const_slice(0, nel_beta_);
  }
  qleve::Tensor<1> spin_squared =
      compute_spin_squared(*resHF_coeffs_, *detSAB_, Cocc_alpha, Cocc_beta, *gammaAB_, *eta_);

  // fmt::print("<S^2> = {:18.8f}\n", spin_squared);
  spin_squared.print("<S^2>");

  if (input_.contains("cube")) {
    fmt::print("Computing electronic state densities\n");
    auto state_dens = compute_electronic_dens(*eta_, *detsig_, *resHF_coeffs_, *gammaAB_);

    fmt::print("Writing cube files\n");
    for (ptrdiff_t state = 0; state < nstates_; state++) {
      std::string filename = "density_state_" + std::to_string(state) + ".cube";
      fmt::print("  --> file {}\n", filename);
      io::write_density_to_cube(input_.get_node("cube"), geometry_, state_dens.pluck(state), filename);
    }
  }

} // end uResHF compute function

///////////////////////////////////////////////////////////////////////////////////////
//      uResHF Class Function Definitions                                            //
///////////////////////////////////////////////////////////////////////////////////////

void uResHF::compute_gradient_impl()
{
  gradient_ = make_unique<qleve::Tensor<2>>(3, geometry_->natoms());
}

bool uResHF::converged(const double dE, const double alpha_error, const double beta_error,
                       const double drho) const
{
  bool conv = (abs(dE) < conv_energy_) && (abs(alpha_error) < conv_error_)
              && (abs(beta_error) < conv_error_) && (abs(drho) < conv_density_);
  return conv;
}

shared_ptr<Wfn> uResHF::wavefunction()
{
  std::array<ptrdiff_t, 2> nel_array;
  nel_array[0] = nel_alpha_;
  nel_array[1] = nel_beta_;
  return make_shared<uResHFReference>(geometry_, rscf_coeffs_, sdets_, resHF_coeffs_, fock_,
                                      state_weight_, nel_array, nmo_, energySA_);
}

qleve::Tensor<1> uResHF::compute_spin_squared(const qleve::ConstTensorView<2>& cAB,
                                              const qleve::ConstTensorView<2>& SAB,
                                              const qleve::ConstTensorView<3>& alpha,
                                              const qleve::ConstTensorView<3>& beta,
                                              const qleve::ConstTensorView<5>& QAB,
                                              const qleve::ConstTensorView<2>& eta) const
{
  const auto [nao, nocca, ndet] = alpha.shape();
  const auto noccb = beta.extent(1);

  assert(nao == beta.extent(0));
  assert(ndet == beta.extent(2));

  const double sz = 0.5 * (nocca - noccb);
  const double nn = sz * sz - sz + nocca;

  qleve::Tensor<2> S_2 = nn * SAB;
  for (ptrdiff_t A = 0; A < ndet; ++A) {
    for (ptrdiff_t B = 0; B < ndet; ++B) {
      auto Qa = QAB.const_pluck(A, B, 0);
      auto QaS = qleve::gemm("n", "n", 1.0, Qa, *overlap_);
      auto Qb = QAB.const_pluck(A, B, 1);
      auto QbS = qleve::gemm("n", "n", 1.0, Qb, *overlap_);


      const double dd = eta(A, B) * qleve::dot_product(QaS, "pq", QbS, "qp");
      S_2(A, B) -= dd;
    }
  }
  S_2.print("<A| S^2 |B>");

  const auto nst = cAB.extent(1);
  qleve::Tensor<2> S2_ad(nst, nst);
  qleve::matrix_transform(1.0, S_2, cAB, 0.0, S2_ad);
  S2_ad.print("<I| S^2 |J>");

  Tensor<1> ss(nst);
  for (ptrdiff_t i = 0; i < nst; ++i) {
    ss(i) = S2_ad(i, i);
  }
  return ss;
}

///////////////////////////////////////////////////////////////////////////////////////
//      FD debugger for uResHF fock matrix                                           //
///////////////////////////////////////////////////////////////////////////////////////

void yucca::fd_fock_ureshf(const shared_ptr<Method_>& method,
                           const shared_ptr<yucca::Geometry>& geometry, const double h,
                           const ptrdiff_t order)
{
  auto ureshf = dynamic_pointer_cast<yucca::uResHF>(method);

  if (!ureshf) {
    throw runtime_error("Dynamic cast of base method to uResHF method failed!");
  }

  // pull out size placeholders
  const ptrdiff_t nao = ureshf->nao();
  const ptrdiff_t nmo = ureshf->nmo();
  const ptrdiff_t nocc_alpha = ureshf->nocc_alpha();
  const ptrdiff_t nocc_beta = ureshf->nocc_beta();
  const ptrdiff_t ndets = ureshf->nSD();

  const double nuclear_repulsion = geometry->nuclear_repulsion();

  // resolve FD stencil requested by user
  static vector<vector<pair<int, double>>> fd_stencils = {
      {{0, -1}, {1, 1}},                                      // 1st order forward difference
      {{-1, -0.5}, {1, 0.5}},                                 // 2nd order central difference
      {{0, -11. / 6.}, {1, 3.}, {2, -3. / 2.}, {3, 1. / 3.}}, // 3rd order forward difference
      {{-2, 1. / 12.},
       {-1, -2. / 3.},
       {1, 2. / 3.},
       {2, -1. / 12.}}, // 4th order central difference
      {{0, -137. / 60.},
       {1, 5.},
       {2, -5.},
       {3, 10. / 3.},
       {4, -5. / 4.},
       {5, 1. / 5.}}, // 5th order forward difference
      {{-3, 1. / 60.},
       {-2, 3. / 20.},
       {-1, -3. / 4.},
       {1, 3. / 4.},
       {2, -3. / 20.},
       {3, 1. / 60.}} // 6th order central difference
  };
  std::vector<std::pair<int, double>> fd_stencil = fd_stencils.at(order - 1);
  // fmt::print("using finite difference stencil: {}\n", fd_stencil);
  fmt::print("  - FD step size requested: {}\n", h);
  fmt::print("  - FD order requested: {}\n", order);
  for (auto [step, coef] : fd_stencil) {
    fmt::print("     {} * f[x_({})]\n", coef, step);
  }

  // function to compute the FD energies for a given orbital rotation
  auto compute_energy = [&](const ptrdiff_t sd, const ptrdiff_t i, const ptrdiff_t a,
                            const string spin, const double r) {
    // make a copy of the uResHF method class
    yucca::uResHF ureshf_test(*ureshf);

    // copy the pointer of test uResHF fockbuilder function
    auto test_uresfockbuilder = ureshf_test.uresfockbuilder();

    std::shared_ptr<qleve::Tensor<4>> test_sd = ureshf_test.sdets();

    if (r == 0) {
      // Generate state-averaged energies with un-rotated sdets with a call to ufockbuilder
      auto empty_fock = (*test_uresfockbuilder)(test_sd->pluck(0), test_sd->pluck(1), true);
      auto state_E = test_uresfockbuilder->energies();
      double test_ESA_total = test_uresfockbuilder->ESA();

      return test_ESA_total;
    } else {
      // apply FD orbital rotation to MOs, dependent on input spin!

      assert(spin == "alpha" || spin == "beta");
      if (spin == "alpha") {
        // rotate selected alpha-spin MOs (sdets_t->pluck(sd))
        qleve::rotate(nao, &(test_sd->pluck(sd, 0))(0, i), &(test_sd->pluck(sd, 0))(0, a), r);
      } else if (spin == "beta") {
        // rotate selected beta-spin MOs
        qleve::rotate(nao, &(test_sd->pluck(sd, 1))(0, i), &(test_sd->pluck(sd, 1))(0, a), r);
      }

      // Generate state-averaged energies with rotated sdets with a call to ufockbuilder
      auto empty_fock = (*test_uresfockbuilder)(test_sd->pluck(0), test_sd->pluck(1), true);
      auto state_E = test_uresfockbuilder->energies();
      double test_ESA_total = test_uresfockbuilder->ESA();

      return test_ESA_total;
    }
  };

  // run finite difference calculation of energy derivative
  // calculate for both alpha spin and beta spin MO rotations
  fmt::print("Running Finite Difference of E_SA derivatives:\n");
  qleve::Tensor<3> dE_e1_alpha(nmo - nocc_alpha, nocc_alpha, ndets);
  qleve::Tensor<3> dE_e2_alpha(dE_e1_alpha.zeros_like());
  qleve::Tensor<3> dESA_alpha(dE_e1_alpha.zeros_like());
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    fmt::print(" - Calculating alpha-spin dE matrix for Slater Determinant #{}\n", sd);
    for (ptrdiff_t iu = 0; iu < nocc_alpha; iu++) {
      for (ptrdiff_t au = nocc_alpha; au < nmo; au++) {
        // compute an energy for each term in FD stencil
        for (auto [step, coef] : fd_stencil) {
          dESA_alpha(au - nocc_alpha, iu, sd) +=
              coef * compute_energy(sd, iu, au, "alpha", step * h) / (2.0 * h);
          // divide by extra factor of 2 to match fock matrices
        }
      }
    }
  }

  // rinse and repeat for beta MOs
  qleve::Tensor<3> dE_e1_beta(nmo - nocc_beta, nocc_beta, ndets);
  qleve::Tensor<3> dE_e2_beta(dE_e1_beta.zeros_like());
  qleve::Tensor<3> dESA_beta(dE_e1_beta.zeros_like());
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    fmt::print(" - Calculating beta-spin dE matrix for Slater Determinant #{}\n", sd);
    for (ptrdiff_t id = 0; id < nocc_beta; id++) {
      for (ptrdiff_t ad = nocc_beta; ad < nmo; ad++) {
        // compute an energy for each term in FD stencil
        for (auto [step, coef] : fd_stencil) {
          dESA_beta(ad - nocc_beta, id, sd) +=
              coef * compute_energy(sd, id, ad, "beta", step * h) / (2.0 * h);
          // divide by extra factor of 2 to match fock matrices
        }
      }
    }
  }

  // pull out fock matrix terms from original (NOT copied!) uResHF method
  auto OG_uresfockbuilder = ureshf->uresfockbuilder();
  auto [OG_aofock_a, OG_aofock_b] = OG_uresfockbuilder->aofock_total();

  // check FD results against original ResHF FOCK matrix

  // cycle through each sdet and compare dE_SA to Fock matrix elements
  double total_error = 0.0;
  for (ptrdiff_t sd = 0; sd < ndets; sd++) {
    auto aoFA_total_a = OG_aofock_a.const_pluck(sd);
    auto aoFA_total_b = OG_aofock_b.const_pluck(sd);

    // initialize MO fock matrix holders
    qleve::Tensor<2> moFA_total_a(nmo, nmo);
    qleve::Tensor<2> moFA_total_b(nmo, nmo);

    // transform fock matrix from AO to MO basis
    qleve::Tensor<2> sao = geometry->overlap();
    fmt::print("Checking orthonormality of MO coeffs for SD#{}:\n", sd);
    qleve::Tensor<2> C_A_a(ureshf->sdets()->const_pluck(sd, 0));
    qleve::Tensor<2> CSC_a(nmo, nmo);
    qleve::matrix_transform(1.0, sao, C_A_a, 0.0, CSC_a);
    fmt::print(" - Alpha CA orthonormal? : {:g}\n", qleve::linalg::ident(CSC_a));

    qleve::matrix_transform("t", "n", 1.0, C_A_a, aoFA_total_a, C_A_a, 0.0, moFA_total_a);

    qleve::Tensor<2> C_A_b(ureshf->sdets()->const_pluck(sd, 1));
    qleve::Tensor<2> CSC_b(nmo, nmo);
    qleve::matrix_transform(1.0, sao, C_A_b, 0.0, CSC_b);
    fmt::print(" - Beta CA orthonormal? : {:g}\n", qleve::linalg::ident(CSC_b));

    qleve::matrix_transform("t", "n", 1.0, C_A_b, aoFA_total_b, C_A_b, 0.0, moFA_total_b);

    // access the appropriate dE_SA matrix
    auto dESA_a = dESA_alpha.const_pluck(sd);
    auto dESA_b = dESA_beta.const_pluck(sd);

    // calculate difference between Fia and dEai
    fmt::print("uFock Matrix Error Norms for SD#{}:\n", sd);
    qleve::Tensor<2> diffAtotal_a(nmo, nocc_alpha);
    for (ptrdiff_t au = nocc_alpha; au < nmo; au++) {
      for (ptrdiff_t iu = 0; iu < nocc_alpha; iu++) {
        diffAtotal_a(au - nocc_alpha, iu) = dESA_a(au - nocc_alpha, iu) - moFA_total_a(iu, au);
        total_error += pow(abs(diffAtotal_a(au - nocc_alpha, iu)), 2);
      }
    }
    fmt::print("  - Alpha-spin Total Fock term error: {}\n", diffAtotal_a.norm());

    qleve::Tensor<2> diffAtotal_b(nmo, nocc_beta);
    for (ptrdiff_t ad = nocc_beta; ad < nmo; ad++) {
      for (ptrdiff_t id = 0; id < nocc_beta; id++) {
        diffAtotal_b(ad - nocc_beta, id) = dESA_b(ad - nocc_beta, id) - moFA_total_b(id, ad);
        total_error += pow(abs(diffAtotal_b(ad - nocc_beta, id)), 2);
      }
    }
    fmt::print("  - Beta-spin Total Fock term error: {}\n", diffAtotal_b.norm());

  } // move to next Sdet

  // add up total error and take square root for an overall norm of Fock error tensor
  total_error = sqrt(total_error);
  fmt::print("Total error across all Fock matrices: {}\n", total_error);
}
