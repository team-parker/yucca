// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/initial_guess.cpp
///
/// Initial guess classes like sum of atomic densities
#include <memory>
#include <tuple>
#include <unordered_map>

#include <fmt/core.h>

#include <qleve/matrix_transform.hpp>

#include <yucca/input/input_node.hpp>
#include <yucca/physical/constants.hpp>
#include <yucca/resmf/converger.hpp>
// #include <yucca/resmf/fock.hpp>
#include <qleve/rotate.hpp>
#include <qleve/svd.hpp>

#include <yucca/resmf/initial_guess.hpp>
#include <yucca/structure/geometry.hpp>
#include <yucca/util/periodictable.hpp>

using namespace std;
using namespace yucca;

/// Performs one or more rotations between 2 molecular orbitals by an angle of theta
/// User input of rotation angles must be in degrees, not radians
/// Will fail if you try to rotate an orbital with itself
/// Will allow you to rotate two occupied orbitals together or two virtual orbitals
void initial_guess::rotate_guess(qleve::ConstTensorView<2> HF,
                                 const vector<tuple<ptrdiff_t, ptrdiff_t, double>>& rotations,
                                 qleve::TensorView<2> SDview)
{
  ptrdiff_t nao = HF.extent(0);
  SDview = HF;

  for (ptrdiff_t irot = 0; irot < rotations.size(); irot++) {
    const auto& [i, a, theta_deg] = rotations[irot];
    fmt::print("    Performing {} degree rotation between orbitals {} and {}\n", theta_deg, i, a);

    if (i == a) {
      throw runtime_error("Unable to perform orbital rotations within the same orbital!");
    }

    double theta_rad = (theta_deg / 180) * physical::pi<double>;

    qleve::rotate(nao, &SDview(0, i), 1, &SDview(0, a), 1, cos(theta_rad), sin(theta_rad));
  }
}

/// Builds | i + a > and | i - a > out of a set of NTO pairs from a single CIS state
void initial_guess::CIS_guess(qleve::ConstTensorView<2> HF,
                              qleve::ConstTensorView<2> CIS_excitations, const ptrdiff_t& cis,
                              const ptrdiff_t& nnto, const ptrdiff_t& nocc,
                              qleve::TensorView<3> SDview)
{
  ptrdiff_t nao = HF.extent(0);
  ptrdiff_t nmo = HF.extent(1);
  ptrdiff_t nvir = nmo - nocc;

  qleve::Tensor<2> T(CIS_excitations.const_pluck(cis).const_reshape(nocc, nvir));
  fmt::print("    CIS excited state S{} accessed\n", cis + 1);
  fmt::print("    Will be used to build {} Slater determinants\n", SDview.extent(2));

  // Calculate NTOs of selected CIS state
  auto [lambda, U, Vt] = qleve::linalg::svd(T);

  // Transform HF orbitals into NTO orbitals
  qleve::Tensor<2> HF_NTO = HF.zeros_like();
  auto coeffs_occ = HF.const_slice(0, nocc);
  auto coeffs_vir = HF.const_slice(nocc, nmo);
  qleve::gemm("n", "n", 1.0, coeffs_occ, U, 0.0, HF_NTO.slice(0, nocc));
  qleve::gemm("n", "t", 1.0, coeffs_vir, Vt, 0.0, HF_NTO.slice(nocc, nmo));

  // Begin cycling through all requested NTO pairs for given CIS state
  ptrdiff_t det = 0;
  for (ptrdiff_t nto = 0; nto < nnto; nto++, det += 2) {

    // Check that requested NTO pairs exist
    if (nto >= lambda.size()) {
      throw runtime_error("Requested NTO pair exceeds number of NTO pairs available!");
    }

    // Build linear combinations from requested NTO and store in SDview
    // Each NTO pair generates two Slater determinants
    const double root2 = 1.0 / std::sqrt(2.0);

    fmt::print("    Calculating i + a linear combo from NTO pair #{}\n", nto);
    SDview.pluck(det) = HF_NTO;
    qleve::rotate(nao, &SDview.pluck(det)(0, nto), 1, &SDview.pluck(det)(0, nocc + nto), 1, root2,
                  root2);
    fmt::print("    Stored in SD slice slot #{}\n", det);

    fmt::print("    Calculating i - a linear combo from NTO pair #{}\n", nto);
    SDview.pluck(det + 1) = HF_NTO;
    qleve::rotate(nao, &SDview.pluck(det + 1)(0, nto), 1, &SDview.pluck(det + 1)(0, nocc + nto), 1,
                  root2, -1.0 * root2);
    fmt::print("    Stored in SD slice slot #{}\n", det + 1);
  }
}
