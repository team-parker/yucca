// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file src/resmf/ureshf_1rdm.cpp
///
/// Implementation for 1RDM and 1TDM calculations for unrestricted ResHF
/// Also electronic state densities, because it's kinda similar?

#include <yucca/resmf/ureshf.hpp>

using namespace yucca;
using namespace qleve;
using namespace std;

qleve::Tensor<3> uResHF::compute_1rdms(const qleve::ConstTensorView<5> Q_AB,
                                      const qleve::ConstTensorView<2> C_AB) const
{
  const ptrdiff_t ndet = C_AB.extent(0);
  const ptrdiff_t nst = C_AB.extent(1);
  const ptrdiff_t nao = Q_AB.extent(0);
  assert(nao == Q_AB.extent(1));
  assert(Q_AB.extent(2) == ndet && Q_AB.extent(3) == ndet);
  assert(Q_AB.extent(4) == 2);

  Tensor<3> out(nao, nao, nst);

  auto qab_alpha = Q_AB.const_pluck(0);
  auto qab_beta = Q_AB.const_pluck(1);
  for (ptrdiff_t istate = 0; istate < nst; ++istate) {
    for (ptrdiff_t A = 0; A < ndet; ++A) {
      for (ptrdiff_t B = 0; B < ndet; ++B) {
        const double c = C_AB.at(A, istate) * C_AB.at(B, istate);
        out.pluck(istate) += c * qab_alpha.const_pluck(A, B);
        out.pluck(istate) += c * qab_beta.const_pluck(A, B);
      }
    }
  }

  return out;
}

qleve::Tensor<3> uResHF::compute_1tdms(const qleve::ConstTensorView<5> Q_AB,
                                      const qleve::ConstTensorView<2> C_AB) const
{
  const ptrdiff_t ndet = C_AB.extent(0);
  const ptrdiff_t nst = C_AB.extent(1);
  const ptrdiff_t nao = Q_AB.extent(0);
  assert(nao == Q_AB.extent(1));
  assert(Q_AB.extent(3) == ndet && Q_AB.extent(2) == ndet);
  assert(Q_AB.extent(4) == 2);

  const ptrdiff_t npairs = nst * (nst - 1) / 2;

  Tensor<3> out(nao, nao, npairs);

  auto qab_alpha = Q_AB.const_pluck(0);
  auto qab_beta = Q_AB.const_pluck(1);
  for (ptrdiff_t jstate = 0, ij = 0; jstate < nst; ++jstate) {
    for (ptrdiff_t istate = 0; istate < jstate; ++istate, ++ij) {
      for (ptrdiff_t A = 0; A < ndet; ++A) {
        for (ptrdiff_t B = 0; B < ndet; ++B) {
          const double c = C_AB.at(A, istate) * C_AB.at(B, jstate);
          out.pluck(ij) += c * qab_alpha.const_pluck(A, B);
          out.pluck(ij) += c * qab_beta.const_pluck(A, B);
        }
      }
    }
  }

  return out;
}

qleve::Tensor<3> uResHF::compute_electronic_dens(const qleve::ConstTensorView<2> eta, 
                                                const qleve::ConstTensorView<3> detsig,
                                                const qleve::ConstTensorView<2> C_AI,
                                                const qleve::ConstTensorView<5> gamma_AB) const
{
  const ptrdiff_t ndet = C_AI.extent(0);
  const ptrdiff_t nst = C_AI.extent(1);
  const ptrdiff_t nao = gamma_AB.extent(0);
  assert(nao == gamma_AB.extent(1));
  assert(gamma_AB.extent(3) == ndet && gamma_AB.extent(2) == ndet);
  assert(gamma_AB.extent(4) == 2);
  assert(eta.extent(0) == ndet && eta.extent(1) == ndet);
  assert(detsig.extent(0) == ndet && detsig.extent(1) == ndet);
  assert(detsig.extent(2) == 2);

  // Compute the electronic state density
  // Important note:
  // gammaAB_alpha = |Sigma_alpha| * QAB_alpha
  // (missing: |Sigma_beta| * |U_alpha V_alpha| * |U_beta V_beta|)
  Tensor<3> rho(nao, nao, nst);
  Tensor<2> dens_a(nao, nao);
  Tensor<2> dens_b(nao, nao);
  for (ptrdiff_t state = 0; state < nst; state++) {
    for (ptrdiff_t A = 0; A < ndet; A++) {
      for (ptrdiff_t B = 0; B < ndet; B++) {
        dens_a = eta(A, B) * detsig(A, B, 1) * gamma_AB.const_pluck(A, B, 0);
        dens_b = eta(A, B) * detsig(A, B, 0) * gamma_AB.const_pluck(A, B, 1);

        rho.pluck(state) += (dens_a + dens_b) * C_AI(A, state) * C_AI(B, state);
      }
    }
  }

  return rho;
}
