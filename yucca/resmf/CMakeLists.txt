# add .cpp files here as you make them
set(RESMF_SOURCES
    reshf.cpp reshf_1rdm.cpp converger.cpp initial_guess.cpp hessian.cpp fock.cpp fdnr.cpp
    ureshf.cpp ureshf_1rdm.cpp ufock.cpp uconverger.cpp)

add_library(resmf ${RESMF_SOURCES})
target_link_libraries(resmf PUBLIC util qleve Eigen3::Eigen Boost::headers Libint2::int2 fmt::fmt)
