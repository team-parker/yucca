// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/uconverger.hpp
///
/// Interface file for convergence acceleration like DIIS, UHF
#pragma once

#include <memory>

#include <qleve/array.hpp>
#include <qleve/tensor.hpp>

#include <yucca/scf/converger.hpp>

namespace yucca
{

// fwd declare
class InputNode; // #include <yucca/input/input_node.hpp>

class UResHFConverger : public SCFConverger {
 protected:
  std::array<ptrdiff_t, 2> nelec_; ///< number of occupied orbitals

  std::unique_ptr<qleve::Tensor<4>> coeffs_; ///< reference set of coeffs
  std::unique_ptr<qleve::Tensor<2>> kappa_;  ///< rotation matrix for bfgs

 public:
  UResHFConverger(const InputNode& inp, const std::array<ptrdiff_t, 2>& nelec);

  void push(const qleve::TensorView<4>& fock, const qleve::TensorView<4>& err, double energy);

  /// next_coeff should be the current set of coeffs
  void next(qleve::TensorView<4> next_coeff);
};

} // namespace yucca
