// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file yucca/resmf/fdnr.hpp
///
/// Finite Difference Newton-Raphson interface

#include <fmt/core.h>
#include <fmt/ranges.h>

#include <qleve/matrix_transform.hpp>
#include <qleve/unitary_exp.hpp>

#include <yucca/resmf/fdnr.hpp>


using namespace std;
using namespace yucca;

static const bool verbose = false;

FDNR_func::FDNR_func(const qleve::ConstTensorView<3> sd, const qleve::ConstTensorView<3> F,
                     const shared_ptr<ResFockBuilder>& builder, const int fd_order,
                     const double h) :
    initial_sdets_(make_shared<qleve::Tensor<3>>(sd)),
    initial_fock_(make_shared<qleve::Tensor<3>>(F)),
    resfockbuilder_(builder),
    scale_(h)
{
  nSD_ = sd.extent(2);
  nmo_ = sd.extent(1);
  nocc_ = builder->nocc();
  nvir_ = nmo_ - nocc_;

  assert(F.extent(0) == nmo_ && F.extent(1) == nmo_ && F.extent(2) == nSD_);

  shifted_sdets_ = make_shared<qleve::Tensor<3>>(sd.zeros_like());
  shifted_fock_ = make_shared<qleve::Tensor<3>>(F.zeros_like());

  assert(shifted_fock_->extent(0) == nmo_ && shifted_fock_->extent(1) == nmo_
         && shifted_fock_->extent(2) == nSD_);
  assert(shifted_fock_->size() == shifted_sdets_->size());

  static vector<vector<pair<int, double>>> fd_stencils = {
      {{0, -1}, {1, 1}},                                      // 1st order forward difference
      {{-1, -0.5}, {1, 0.5}},                                 // 2nd order central difference
      {{0, -11. / 6.}, {1, 3.}, {2, -3. / 2.}, {3, 1. / 3.}}, // 3rd order forward difference
      {{-2, 1. / 12.},
       {-1, -2. / 3.},
       {1, 2. / 3.},
       {2, -1. / 12.}}, // 4th order central difference
      {{0, -137. / 60.},
       {1, 5.},
       {2, -5.},
       {3, 10. / 3.},
       {4, -5. / 4.},
       {5, 1. / 5.}}, // 5th order forward difference
      {{-3, 1. / 60.},
       {-2, 3. / 20.},
       {-1, -3. / 4.},
       {1, 3. / 4.},
       {2, -3. / 20.},
       {3, 1. / 60.}} // 6th order central difference
  };
  fd_stencil_ = fd_stencils.at(fd_order - 1);
  if (verbose)
    fmt::print("using finite difference stencil: {}\n", fd_stencil_);
}

void FDNR_func::operator()(const qleve::ConstTensorView<2>& V, qleve::TensorView<2> W)
{
  assert(V.size() == W.size());
  assert(V.size() == nvir_ * nocc_ * nSD_);

  auto compute_step = [&](const int i) -> qleve::Tensor<3> {
    auto iFockia = initial_fock_->subtensor({nocc_, 0, 0}, {nmo_, nocc_, nSD_});
    if (i == 0) {
      if (verbose)
        fmt::print("Fia({:d}h) = {}\n", i, iFockia.norm());
      return iFockia;
    } else {
      kappa_ = make_shared<qleve::Tensor<3>>(scale_ * static_cast<double>(i)
                                             * V.const_reshape(nvir_, nocc_, nSD_));

      shift_sdets();
      (*resfockbuilder_)(*shifted_sdets_, *shifted_fock_);

      // convert to MO fock
      qleve::Tensor<3> Fmo(nmo_, nmo_, nSD_);
      for (std::ptrdiff_t SD_A = 0; SD_A < nSD_; SD_A++) {
        auto FA_ao = shifted_fock_->const_pluck(SD_A);
        auto FA_mo = Fmo.pluck(SD_A);
        auto sdA = shifted_sdets_->const_pluck(SD_A);

        qleve::matrix_transform(1.0, FA_ao, sdA, 0.0, FA_mo);
      }


      auto sFockia = Fmo.subtensor({nocc_, 0, 0}, {nmo_, nocc_, nSD_});
      if (verbose)
        fmt::print("Fia({:d}h) = {}\n", i, sFockia.norm());
      return sFockia;
    }
  };

  if (verbose)
    fmt::print("\n");

  qleve::Tensor<3> out(nvir_, nocc_, nSD_);
  for (auto [i, coef] : fd_stencil_) {
    if (coef != 0.0) {
      out += coef * compute_step(i);
    }
  }

  W = (1 / scale_) * out.reshape(out.size(), 1);
}

void FDNR_func::shift_sdets()
{
  for (std::ptrdiff_t SD_A = 0; SD_A < nSD_; SD_A++) {
    // turn dX (kappa_) into a unitary matrix (MO rotation matrix!)
    auto kappaA_ai = kappa_->const_pluck(SD_A);
    auto UA = qleve::linalg::unitary_exp(kappaA_ai);

    // rotate each sdet and store into shifted_sdets_
    auto sdetA_old = initial_sdets_->const_pluck(SD_A);
    auto sdetA_new = shifted_sdets_->pluck(SD_A);
    gemm("n", "n", 1.0, sdetA_old, UA, 0.0, sdetA_new);
  }
}
